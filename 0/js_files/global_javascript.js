//parallax effect for sidebar and navigation 

    var previous_scroll_location=0;
	var nav_height=$('#nav-wrapper').css('height').split('px')[0];

    $(window).on('resize scroll',function(){
      var nav_width=document.getElementById('nav-wrapper').offsetWidth;
      slide_item_auto_size(nav_width);
    });

  function slide_item_auto_size(nav_width){
    var item=document.getElementsByClassName('slide-item');
    if(nav_width>400 && nav_width<700){
      $('main').css({'margin-left':'0px', 'margin-right':'0px'});
      $('.product-slide-inner').css('height:250px');
      for(i=0; i<item.length;i++){
        item[i].style.width='200px';
      }
    }else if(nav_width>700 && nav_width<1000){
		$('.product-slide-inner').css('height:250px');
		for(i=0; i<item.length;i++){
		item[i].style.width='200px';
		}
    }else if(nav_width>1000){
      $('main').css({'padding-left':'20px', 'padding-right':'20px'});
	   $('.product-slide-inner').css('height:300px');
      for(i=0; i<item.length;i++){
        item[i].style.width='250px';
      }
      }
    }


//enable swipe

var box = document.getElementsByClassName('products-slide');
var start_pos=0;
var move_dist=0;
var end_pos=0;
window.addEventListener('touchstart', function(e){
  var touchObj = e.changedTouches[0];
  start_pos = parseInt(touchObj.clientX);
    console.log("strt "+start_pos);
}, false);
var prev_displacement=0;
var box_width = parseInt($('.slide-item').first().css('width'));
var total_items = $('.slide-item').length;
var total_width = total_items * box_width;
window.addEventListener('touchmove', function(e){
  var touchObj = e.changedTouches[0];
  move_dist = parseInt(touchObj.clientX)-start_pos;
  prev_displacement+=move_dist;
    console.log("mov "+prev_displacement +" max "+(total_width*-1));
    if(move_dist>0){
      if(prev_displacement<0){
        if(prev_displacement<(total_width))
          $('.slide-item').css({'transform':'translate('+(prev_displacement+move_dist)+'px, 0px)','transition':'all .3s linear'});
        if(prev_displacement>total_width)
        $('.slide-item').css({'transform':'translate('+total_width+'px, 0px)','transition':'all .3s linear'});
          prev_displacement=total_width+1;
      }else{
        $('.slide-item').css({'transform':'translate(0px, 0px)','transition':'all .3s linear'});
        prev_displacement=0;
      }
    }else if (move_dist<0) {
      if(prev_displacement>=(total_width*-1))
        $('.slide-item').css({'transform':'translate('+(prev_displacement+move_dist)+'px, 0px)','transition':'all .3s linear'});
      if(prev_displacement<(total_width*-1))
        prev_displacement=(total_width*-1)-1;
    }
}, false);

window.addEventListener('touchend', function(e){
  var touchObj = e.changedTouches[0];
  end_pos = parseInt(touchObj.clientX);
  console.log("strt "+start_pos+" end "+end_pos);
  var position_fixer = prev_displacement%box_width;
  var fix_margin = box_width - position_fixer;
  //$('.slide-item').css({'transform':'translate('+(prev_displacement+fix_margin)+'px, 0px)','transition':'all .3s linear'});

  console.log("fixer "+position_fixer+ " box_width "+ box_width+" prev_displacement "+prev_displacement);
});



//removes carousel indicators on touch enabled devices
  if('ontouchstart' in document.documentElement){
    $('.slide-indicators').remove();
  }else{
  }



//custom carousel settings 

$(window).ready(function(){

  var prev_translation=0;
  var magnitude=parseInt($('.slide-item').first().css('width'));
  var total_items = $('.slide-item').length;
  var total_width = total_items * magnitude;
  var box_width = parseInt($('.products-slide').css('width'));
  var max_translation = box_width - total_width;

  $(window).on('load resize',function(){
    magnitude=parseInt($('.slide-item').first().css('width'));
    total_width = total_items * magnitude;
  });
    $('.slide-control-prev').on('click', function(){
      if(prev_translation<0)
        prev_translation += magnitude;
      slide_control('left','slide-item',prev_translation, total_items, magnitude, max_translation);
      });
      $('.slide-control-next').on('click', function(){
        if(prev_translation>max_translation)
          prev_translation -= magnitude;
        slide_control('right','slide-item',prev_translation, total_items, magnitude, max_translation);
        });

        function slide_control(direction,id,ps,ic,iw,ms){
          console.log(iw);
          prev_translation =  parseInt($('#produce .'+id).css('left'));
          if(direction=="right"){
            if(Math.abs(prev_translation)<=Math.abs(ms)){
              $('#produce .'+id).css({
                'left':prev_translation-iw+'px',
                'transition':'all .3s linear'
              });
            }
          }
          if(direction=="left"){
            if(prev_translation!=0){
              $('#produce .'+id).css({
                'left':prev_translation+iw+'px',
                'transition':'all .3s linear'
              });
            }
          }
        }
});


// add to cart handler 

$('.add-to-cart-btn').click(function(e){
  $(window).ready(function(){

    $.ajax({
      url:"food & groceries.php",
      method:"POST",
      data: {qty:qty, size:size, product:product},
      success:function(data){
          alert("success");
      }
    });
  });
});

var open="departments";
//controls fixed navigation visibility
$('.breadcrumb li').on('click', function(){
	if(open==$(this).attr('id')){
		document.getElementById('navigation-dropdown').classList.toggle("hidden");
	}else{
	    open = $(this).attr('id');
	}
});