<?php
include("../../connect.php");
$dp_arr = array();
$depName;
if(isset($_POST['depName']) && !empty($_POST['depName'])){
  $depName = $_POST['depName'];
  /*
  if($_POST['depParent']=='None'){
    $depParent = '0'
  }else{
    $dp_tier_sql="SELECT tier FROM departments WHERE depCode=".$_POST['depParent']";
    $dp_tier_query=mysqli_query($connector, $dp_tier_sql);
    $depTier=$dp_tier_query[0];
  }
  */
  if(isset($_POST['depParent'])){
    if(isset($_POST['depCode'])){
        $dp_code_sql="SELECT departmentCode FROM departments WHERE departmentCode='$depCode'";
        $dp_code_query = mysqli_query($connector,$dp_code_sql);
        //checks if department exists
        if (mysqli_num_rows($dp_code_query)!=0){
            $dp_edit_sql="UPDATE departmentName,parent FROM departments";
          } else{
          //sets new department
            if(isset($_POST['depChild'])){
              $dp_add_sql="INSERT INTO departments(`departmentName`,`parent`,`departmentCode`,`child`,`tier`)
              VALUES (``,``,``,``)";
            }else{
              $dp_add_with_child_sql="INSERT INTO departments()";
          }
        }
    }
  }
}

$department_sql="SELECT departmentName,tier,child,departmentCode FROM departments";
$department_query=mysqli_query($connector,$department_sql);
?>
<html lang="en" dir="ltr">
  <head>
    <meta charset="utf-8">
    <?php include_once("../../headTags.php"); ?>
    <title></title>
    <style media="screen">
      .clickable:hover{
        cursor:pointer;
      }


      div #depParentOptions{
        border: solid 1px silver;
        border-radius: 5px 5px;
        box-shadow: 0.4px 0.4px 1px silver;
      }

      div #depParentOptions ul{
        margin:0px 0px;
      }

      div #depParentOptions ul li:hover{
        background-color: silver;
        cursor:pointer;
      }

      .fixed-side-left{
        position:fixed;
        width:33.33%;
      }
    </style>
  </head>
  <body>
    <?php include_once("includes/header.php") ?>
    <main class="container-fluid">
      <div class="row">
        <div class="col-sm-4 fixed-side-left">
          <form class="sticky-top" action="departmentsWorkspace.php" method="post">
            <div class="form-group">
              <label for="depName">Name</label>
              <input type="text" name="depName" value="<?= (isset($depName)?$depName:''); ?>" class="form-control">
            </div>
            <div class="form-group">
              <label for="depParent">Parent</label>
              <input type="text" name="depParent" value="<?= (isset($depParent)?$depParent:''); ?>" class="form-control">
              <div id="depParentOptions">
                <ul class="list-unstyled">
                  <li class="depParentSgt">fdasr</li>
                  <li class="depParentSgt">gobo</li>
                </ul>
              </div>
            </div>
            <div class="form-group">
              <label for="depCode">Code</label>
              <input type="text" name="depCode" value="<?= (isset($depCode)?$depCode:''); ?>" class="form-control">
            </div>
            <div class="form-group">
              <input type="submit" name="" value="ADD" class="btn">
              <input type="reset" name="" value="CANCEL" class="btn hidden">
            </div>
          </form>
        </div>
        <div class="col-sm-8 col-sm-offset-4">
          <table class="table">
            <?php
              while($department_results=mysqli_fetch_assoc($department_query)):
                $name='';
                $dp_arr=$department_results['departmentName'];
                $department_results['tier'];
                if($department_results['tier']==0):
                  $name=$department_results['departmentName'];
            ?>
            <tr>
              <td>
                <div class="container-fluid">
                  <div class="row hover-control">
                    <div class="col-sm-4">
                      <h3 class="clickable depList" onclick="toggleDepartments(<?=str_replace(['\'',' ','&'],'',$name)?>_sub)"><?=$name?></h3>
                    </div>
                    <div class="col-sm-1 offset-7">
                      <span class="glyphicon glyphicon-edit clickable hover-display hidden" onclick="editDepName('<?=$name?>')" style="margin-top:8px;"></span>
                    </div>
                  </div>
                </div>
                <?php
                  if($department_results['child']==1):
                    $code=$department_results['departmentCode'];
                    $sub_department_sql="SELECT departmentName,tier,child,departmentCode,parent FROM departments WHERE parent='$code'";
                    $sub_department_query=mysqli_query($connector,$sub_department_sql);
                ?>
                <hr>
                <div class="container-fluid hidden" id="<?=str_replace(['\'',' ','&'],'',$name)?>_sub">
                  <div class="row">
                    <ul class="list-unstyled" style="width:100%;">
                      <?php
                          while($sub_department_results=mysqli_fetch_assoc($sub_department_query)):
                            $sub_name=$sub_department_results['departmentName'];
                            $dp_arr=$sub_name;
                      ?>
                      <li>
                        <div class="container-fluid">
                          <div class="row hover-control">
                            <div class="col-sm-4 offset-1">
                              <h4 class="clickable depList" onclick="toggleDepartments(<?=str_replace(['\'',' ','&'],'',$sub_name)?>_sub)"><?=$sub_name?></h4>
                            </div>
                            <div class="col-sm-1 offset-6">
                              <span class="glyphicon glyphicon-edit clickable hidden hover-display" onclick="editDepName('<?=$sub_name?>')" ></span>
                            </div>
                          </div>
                        </div>
                        <?php
                          if($sub_department_results['child']==1):
                            $code2=$sub_department_results['departmentCode'];
                            $sub2_department_sql="SELECT departmentName FROM departments WHERE parent='$code2'";
                            $sub2_department_query=mysqli_query($connector,$sub2_department_sql);
                        ?>
                        <hr>
                        <div class="container-fluid hidden" id="<?=str_replace(['\'',' ','&'],'',$sub_name)?>_sub">
                          <div class="row">
                            <div class="col-sm-10 offset-2">
                              <ul class="list-unstyled" style="width:100%;">
                                <?php
                                    while($sub2_department_results=mysqli_fetch_assoc($sub2_department_query)):
                                      $sub2_name=$sub2_department_results['departmentName'];
                                      $dp_arr=$sub2_name;
                                ?>
                                <li>
                                  <div class="container-fluid">
                                    <div class="row hover-control">
                                      <div class="col-sm-4">
                                        <span class="clickable depList"><?=$sub2_name?></span>
                                      </div>
                                      <div class="col-sm-1 offset-7">
                                        <span class="glyphicon glyphicon-edit clickable hidden hover-display"></span>
                                      </div>
                                    </div>
                                  </div>
                                </li>
                                <?php
                                  endwhile;
                                ?>
                              </ul>
                            </div>
                          </div>
                        </div><hr/>
                        <?php endif; ?>
                      </li>
                      <?php
                        endwhile;endif;
                      ?>
                    </ul>
                  </div>
                </div>
                <?php  endif; ?>
              </td>
            </tr>
          <?php endwhile; ?>
          </table>
        </div>
      </div>
    </main>
<?php include_once($_SERVER['DOCUMENT_ROOT'].'\website\scriptTags.php')?>
    <script type="text/javascript">
      function editDepName(id){
        $("input[name='depName']").val(id);
        $("input[type='submit']").val("SAVE");
        $("input[type='reset']").removeClass("hidden");

        // $.ajax({
        //   url: "departmentsWorkspace.php",
        //   method: "post",
        //   data: {id,id},
        //   success: function(data, status){
        //     alert(data);
        //   }
        // });
      }
    </script>
    <script type="text/javascript">

      function toggleDepartments(id){
        id.classList.toggle('hidden');
      }
    </script>
    <script type="text/javascript">
      $(document).ready(function(){
        $('.hover-control').hover(function(){
          $(this).find('.hover-display').toggleClass('hidden').fadeIn('1000');
        });
      });
    </script>
    <script type="text/javascript">
      $(document).ready(function(){
        $('.depParentSgt').click(function(){
          $("input[name=depParent]").val($(this).text());
          $('.depParentSgt').fadeOut(300);
        });
      });
    </script>
  </body>
</html>
