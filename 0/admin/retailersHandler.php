<?php
include("../../connect.php");
  if(isset($_POST['branch_info'])):
    $branch_info_sql="SELECT branch_name,email,telephone,manager FROM branches WHERE branch_code='ms23454'";
    $branch_info_query=mysqli_query($connector,$branch_info_sql);
    if(mysqli_num_rows($branch_info_query)!=0):
      $branch_info_results=mysqli_fetch_assoc($branch_info_query);
      $branch_mngr_sql="SELECT firstname, lastname FROM user_info WHERE id=\"".$branch_info_results['manager']."\"";
      $branch_mngr_query=mysqli_query($connector,$branch_mngr_sql);
?>
  <div class="container-fluid">
    <div class="row">
      <div class="col-sm-12 text-center">
        <h4 id="rt-name"><?= $branch_info_results['branch_name'];?></h4>
      </div>
    </div>
    <div class="row">
      <div class="col-sm-12 text-center">
        <span id="br-location">Wernhill,Town</span>
      </div>
    </div>
  </div><hr>
  <div class="container-fluid">
    <div class="row">
      <div class="col-sm-6">
        <ul class="list-unstyled">
          <?php
            if(mysqli_num_rows($branch_mngr_query)!=0):
              $branch_mngr_results=mysqli_fetch_assoc($branch_mngr_query);
          ?>
          <li id="br-mgr">Manager: <?= $branch_mngr_results['firstname']." ".$branch_mngr_results['lastname']?></li>
          <li id="br-admin">Administrator:</li>
          <li id="br-hos">Head Of Sales:</li>
        <?php endif; ?>
        </ul>
      </div>
      <div class="col-sm-6">
        <div class="container-fluid">
          <div class="row">
            <div class="col-sm-6 offset-6">
              <ul class="list-unstyled">
                <li>Contact</li>
                <li>Tel: <span><?='0'.$branch_info_results['telephone']?></span></li>
                <li>Instagram: <span></span></li>
                <li>email: <span><?=$branch_info_results['email']?></span></li>
              </ul>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div><hr>
  <div class="container-fluid">
    <div class="row">
      <div class="col-sm-12">
        <table class="table table-hover">
          <caption>Advertisements</caption>
          <thead>
            <tr>
              <td>Product Code</td>
              <td>Product Name</td>
              <td>Price</td>
              <td>Duration</td>
            </tr>
          </thead>
          <tbody>
            <tr>
              <td></td>
              <td></td>
              <td></td>
              <td></td>
            </tr>
          </tbody>
        </table>
      </div>
    </div>
  </div>
<?php
    endif;
  endif;
?>
