<?php
include("../includes/connect.php");
$retailer_sql="SELECT name, retailer_code FROM retailers";
$retailer_query=mysqli_query($connector,$retailer_sql);
?>
<html lang="en" dir="ltr">
  <head>
    <meta charset="utf-8">
    <?php include_once("../includes/headTags.php"); ?>
    <title></title>
    <style media="screen">
      .clickable:hover{
        cursor:pointer;
      }

      caption {
        text-align: center;
        caption-side: top;
      }
    </style>
  </head>
  <body>
    <?php include_once("includes/header.php") ?>
    <main class="container-fluid">
      <div class="row">
        <div class="col-sm-3" style="position:fixed; width:25%;">
          <ul class="list-unstyled">
            <?php
            while($retailer_results=mysqli_fetch_assoc($retailer_query)):

            ?>
            <li>
              <div>
                <span id="markhams" onclick="toggleRetailers(<?= str_replace(['\'',' '],'',$retailer_results['name'])?>Branches)" class="clickable">
                  <span style="height:5px;width:3px;">▶</span><h4 style="display:inline;"><?= $retailer_results['name'] ?></h4>
                </span>
              </div>
              <?php
              $code=substr($retailer_results['retailer_code'],0,2);
              $branch_sql="SELECT branch_name FROM branches WHERE branch_code LIKE '$code%'";
              //$branch_sql="SELECT SUBSTRING(branch_name,0,2) FROM branches AS name WHERE name='$code'";
              $branch_query=mysqli_query($connector,$branch_sql);
              if(mysqli_num_rows($branch_query)):
              ?>
              <div class="container-fluid hidden" id="<?= str_replace(['\'',' '],'',$retailer_results['name'])?>Branches">
                <div class="row">
                  <div class="col-sm-11 offset-1">
                    <ul class="list-unstyled">
                      <?php

                      while($branch_results=mysqli_fetch_assoc($branch_query)):
                      ?>
                      <li class="clickable sltd-branch" ><?=$branch_results['branch_name']?></li>

                      <?php
                        endwhile;
                      ?>
                    </ul>
                  </div>
                </div>
              </div>
            <?php endif; ?>
            </li>
          <?php endwhile; ?>
          </ul>
        </div>
        <?php //include_once("retailersHandler.php"); ?>
        <div class="col-sm-9 col-sm-offset-3" id="info_space">
        </div>
      </div>
    </main>
    <?php include_once("../includes/scriptTags.php"); ?>
    <script type="text/javascript">

      function toggleRetailers(id){
        id.classList.toggle('hidden');
      }
    </script>
    <script type="text/javascript">

        $('.sltd-branch').click(function(){
          $("#info_space").load('retailersHandler.php',{branch_info:"yat"});
          // $.post('retailersHandler.php', {branch_info:"yat"}, function(data, status){
          //     $('#info_space').removeClass('hidden');
          // });
        });

    </script>
  </body>
</html>
