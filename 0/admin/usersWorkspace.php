<!DOCTYPE html>
<html lang="en" dir="ltr">
  <head>
    <meta charset="utf-8">
    <?php include_once("../includes/headTags.php"); ?>
    <title></title>
    <style media="screen">
      .clickable:hover{
        cursor:pointer;
      }

    </style>
  </head>
  <body>
    <?php include_once("includes/header.php") ?>
    <main class="container-fluid">
      <div class="row">
        <div class="col-sm-3">
          <ul class="list-unstyled">
            <li class="clickable">New Sign Ups</li>
            <li class="clickable">Privileged Users</li>
            <li class="clickable">Active Users</li>
            <li class="clickable">Chart</li>
          </ul>
        </div>
        <div class="col-sm-9">
          <table class="table">
            <thead>
              <tr>
                <td>Username</td>
                <td>Name</td>
                <td>email</td>
                <td>contact</td>
                <td>last login</td>
              </tr>
            </thead>
          </table>
        </div>
      </div>
    </main>
  </body>
</html>
