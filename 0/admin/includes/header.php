<nav class="navbar navbar-light bg-light navbar-fixed-top">
  <div class="col-xs-12 navbar-expand-md">
    <div class="navbar-header">
      <a class="navbar-brand" href="index.php">Admin Panel</a>
      <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#navbarNavDropdown">
          <span class="navbar-toggler-icon"></span>
      </button>
    </div>
    <div class="collapse navbar-collapse" id="navbarNavDropdown">
    <div class="container-fluid">
      <div class="row" style="padding-left:0px;margin-top:4px;">
        <div class="col-sm-12">
          <ul class="nav nav-pills nav-left">
            <li class=""><a class="" href="departmentsWorkspace.php">Departments</a></li>
            <li class=""><a class="" href="productsWorkspace.php">Products</a></li>
            <li class=""><a class="" href="retailersWorkspace.php">Retailers</a></li>
            <li class=""><a class="" href="usersWorkspace.php">Users</a></li>
            <li class=""><a class="" href="ordersWorkspace.php">Orders</a></li>
            <li class=""><a class="" href="salesWorkspace.php">Sales</a></li>
          </ul>
        </div>
      </div>
    </div>
    </div>
  </div>
</nav>
