<?php
require_once("../../connect.php");
$errors = array();
$base_url=$_SERVER['DOCUMENT_ROOT'];
$uplpth="../";
$allowedImgTypes= array("png","jpeg","jpg");
$prID=$_POST['prodID'];
$prNa=$_POST['prodName'];
$prCat=$_POST['prodCateg'];
$prBr=$_POST['prodBrand'];
$prRet=$_POST['prodRetailer'];
$prPr=$_POST['prodPrice'];
$prQty=$_POST['prodQty'];
$prDe=$_POST['prodDescr'];
if(isset($prID) && !empty($prID)){
  //either updating or editing
  if(isset($prNa) && !empty($prNa) && isset($prQty) && !empty($prQty)){
    //updating database
    $updateSql="UPDATE products SET quantity=($prQty+quantity) WHERE productID=$prID";
    $updateExec = mysqli_query($connector,$updateSql);
  }else{
    //editing database
    $required=array("prodID","prodName");
    foreach ($required as $field) {
      if(empty($_POST[$field])){
        $errors[]="All fields are required";
        break;
      }
    }
    if(!empty($errors)){
      echo display_errors($errors);
    }else{

    }
  }
}else{
  //adding new product
  if(isset($_FILES) && !empty($_FILES)){
    //main image handler

    $mimg=$_FILES['prodMainImage'];
    $mname=$mimg['name'];
    $mnaarr=explode('.',$mname);
    $mstrname=$mnaarr[0];
    $mnaext=$mnaarr[1];
    $mmime=$mimg['type'];
    $mmiarr=explode('/',$mmime);
    $mmity=$mmiarr[0];
    $mmiext=$mmiarr[1];
    $msize=$mimg['size'];
    $merr=$mimg['error'];
    $mtmp=$mimg['tmp_name'];

    if($mmity!="image"){
      $errors[]="File type is not image";
    }elseif (!(in_array($mnaext,$allowedImgTypes)) OR !(in_array($mmiext,$allowedImgTypes))) {
      $errors[]="only jpeg, jpg or png's allowed";
    }elseif ($msize > 100000) {
      $errors[]="main image file too large";
    }elseif ($merr != 0){
      $errors[]="error occured while uploading the image";
    }

    //supporting images handler

    $sunames=array();

    $sumimes=array();
    $susizes=array();
    $suerrs=array();
    $sutmps=array();
    $sustrnames=array();
    $sumitys=array();
    $sumiexts=array();
    $suexts=array();
    foreach ($_FILES['prodSuppImages']['name'] as $suna) {
      $sunames[]=$suna;
    }
    foreach ($_FILES['prodSuppImages']['type'] as $sutype) {
      $sumimes[]=$sutype;
    }
    foreach ($_FILES['prodSuppImages']['size'] as $suimgsize) {
      $susizes[]=$suimgsize;
    }
    foreach ($_FILES['prodSuppImages']['error'] as $suimgerr) {
      $suerrs[]=$suimgerr;
    }
    foreach ($_FILES['prodSuppImages']['tmp_name'] as $suimgtmp) {
      $sutmps[]=$suimgtmp;
    }
    for($i=0;$i<sizeof($sunames);$i++){
      //get data of each image
      $sunaarr=explode(".",$sunames[$i]);
      $sustrnames[]=$sunaarr[0];
      $suexts[]=$sunaarr[1];
      $sumiarr=explode("/",$sumimes[$i]);
      $sumitys[]=$sumiarr[0];
      $sumiexts[]=$sumiarr[1];
    }
      //check for errors before upload
      for($i=0;$i<sizeof($sunames);$i++){
        //get data of each image
        $suext=$suexts[$i];
        $sumity=$sumitys[$i];
        $sumiext=$sumiexts[$i];
        $susize=$susizes[$i];
        $suerr=$suerrs[$i];

        //check for errors before upload
        if($sumity!="image"){
          $errors[]="File type is not image";
          break;
        }elseif (!in_array($suext,$allowedImgTypes) OR !in_array($sumiext,$allowedImgTypes)) {
          $errors[]="only jpeg, jpg or png's allowed";
          break;
        }elseif ($susize > 100000) {
          $errors[]="supporting files too large";
          break;
        }elseif ($suerr != 0){
          $erros[]="error occured while uploading the images";
          break;
        }
      }
      if(empty($errors)){
        $muplpth=$uplpth.$mstrname.'.'.$mnaext;
        $suallpths='';
        move_uploaded_file($mtmp, $muplpth);
        for($i=0;$i<sizeof($sutmps);$i++) {
          $suuplpth=$uplpth.$sustrnames[$i].'.'.$suexts[$i];
          $suallpths.=$suuplpth.";";
          move_uploaded_file($sutmps[$i], $suuplpth);
        }
        $addSql = "INSERT INTO products (productID, productName, netPrice, grossPrice,
           retailer, mainImage, supportingImages, brand, categories, description,
             quantity) VALUES (NULL, '$prNa','$prPr','$prPr','$prRet','$muplpth','$suallpths','$prBr','$prCat','$prDe','$prQty')";
        $addExec=mysqli_query($connector,$addSql);
      }
  }else{
      $errors[]="Both images should be provided";
  }

print_r($errors);

}
?>
