<!DOCTYPE html>
<html lang="en" dir="ltr">
  <head>
    <meta charset="utf-8">
    <?php include_once("../includes/headTags.php"); ?>
    <title></title>
    <style media="screen">
      .clickable:hover{
        cursor:pointer;
      }

    </style>
  </head>
  <body>
    <?php include_once("includes/header.php") ?>
    <main class="container-fluid">
      <div class="row">
        <div class="col-sm-3 offset-9">
          <h3>Total: N$ 10000000</h3>
        </div>
      </div>
    </div>
    <div class="container-fluid">
      <div class="row">
        <div class="col-sm-3">
          <ul class="list-unstyled">
            <li class="clickable">Today</li>
            <li class="clickable">This Week</li>
            <li class="clickable">Top Selling</li>
            <li class="clickable">Chart</li>
          </ul>
        </div>
        <div class="col-sm-9">
          <div class="container-fluid">
            <div class="row">
              <table class="table">
                <thead>
                  <tr>
                    <td>Product Code</td>
                    <td>Name</td>
                    <td>Quantity</td>
                    <td>Retailer</td>
                    <td>Amount</td>
                  </tr>
                </thead>
              </table>
            </div>
          </div>
        </div>
      </div>
    </main>
  </body>
</html>
