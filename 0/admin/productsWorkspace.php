<?php
include("../includes/connect.php");
$prodQ="SELECT * FROM products LIMIT 20";
$prodMQ=mysqli_query($connector,$prodQ);

  ?>



<html lang="en" dir="ltr">
  <head>
    <?php include_once("../includes/headTags.php"); ?>
    <title></title>
    <style media="screen">
      .rtts{
        margin-left: 8%;
        padding-top: 10px;

      }
      .asds{
        margin-left: 7%;
      }
      .clickable:hover{
        cursor:pointer;
      }
      #edit-m-img-remove-icon{
        padding: 3px 3px;
      }
      #edit-m-img-remove-icon:hover{
        padding:2px 1px;
      }
      .modal-dialog {
        margin-top: 50px;
      }
    </style>
  </head>
  <body>

    <?php include_once("includes/header.php") ?>
    <main class="container-fluid">
      <div class="btn-group asds">
        <span class="btn btn-default" onclick="toggleActionWindow('view')" id="t01">View</span>
        <span class="btn btn-default" onclick="toggleActionWindow('add')" id="t02">Add</span>
        <span class="btn btn-default" onclick="toggleActionWindow('remove')" id="t03">Remove</span>
        <span class="btn btn-default" onclick="toggleActionWindow('edit')" id="t04">Edit</span>
      </div>
      <div class="row">
        <div class="col-xs-10 rtts">
          <!--product view tab-->
          <table class="table hidden table-hover" id="view">
            <thead>
              <tr>
                <td class="col-xs-1"></td>
                <th>ID</td>
                <th>Name</td>
                <th>Quantity</td>
                <th>Price</td>
              </tr>
            </thead>
            <tbody>
                <?php
                  while($prodR=mysqli_fetch_assoc($prodMQ)):
                    if($prodR['archived']!=1):
                ?>
                <tr id="<?=$prodR['productID']?>">
                <td class="col-xs-1 clickable">
                  <span class="glyphicon glyphicon-edit editPr" onclick="editProd(<?=$prodR['productID']?>);"></span>
                  <span class="glyphicon glyphicon-remove" onclick="setIdRem(<?=$prodR['productID']?>);"></span>
                </td>
                <td><?= $prodR['productID'] ?></td>
                <td><?= $prodR['productName'] ?></td>
                <td><?= $prodR['quantity'] ?></td>
                <td>N$ <?= $prodR['grossPrice'] ?></td>
                </tr>
                <?php
                    endif;
                  endwhile;
                  mysqli_close($connector);
                ?>
            </tbody>
          </table>
          <!--product addition tab-->
          <form class="form hidden" action="productModification.php" method="post" id="add" enctype="multipart/form-data" autofill="off">
            <div class="container-fluid">
              <div class="row">
                <div class="col-xs-2">
                  <div class="form-group">
                    <label for="prodID">ID</label>
                    <input type=text class="form-control" name="prodID" id="prodID">
                  </div>
                  <div class="form-group">
                    <label for="prodCateg">Category</label>
                    <input type=text list="categOptions" class="form-control suggestions" name="prodCateg">
                    <datalist id="categOptions">
                    </datalist>
                  </div>
                  <div class="form-group">
                    <label for="prodBrand">Brand</label>
                    <input type=text list="brandOptions" class="form-control suggestions" name="prodBrand" id="addPbrand">
                    <datalist id="brandOptions">
                    </datalist>
                  </div>
                  <div class="form-group">
                    <label for="prodRetailer">Retailer</label>
                    <input type=text list="retailerOptions" class="form-control suggestions" name="prodRetailer" id="addPretailer">
                    <datalist id="retailerOptions">
                    </datalist>
                  </div>
                </div>
                <div class="col-xs-10">
                  <div class="form-group">
                    <label for="prodName">Name</label>
                    <input type="text" name="prodName" value="" class="form-control" id="addPname">
                  </div>
                  <div class="container-fluid">
                    <div class="row">
                      <div class="form-group col-sm-6">
                        <label for="prodPrice">Price</label>
                        <input type="text" name="prodPrice" value="" class="form-control" id="addPprice">
                      </div>
                      <div class="form-group col-sm-6">
                        <label for="prodQty">Size & Quantity</label>
                        <input type="text" name="prodQty" value="" class="form-control" data-toggle="modal" data-target="#sizeModal" id="addPqty">
                      </div>
                    </div>
                  </div>
                  <div class="form-group">
                    <label for="prodDescr">Description</label>
                    <textarea name="prodDescr" class="form-control" id="addPdescr"></textarea>
                  </div>
                  <div class="container-fluid">
                    <div class="row">
                      <div class="col-sm-6">
                        <div class="form-group">
                          <label for="prodMainImage">Main Image</label>
                          <input type="file" name="prodMainImage" value="" class="form-control">
                        </div>
                      </div>
                      <div class="col-sm-6">
                        <div class="form-group">
                          <label for="prodSuppImages">Supporting Images</label>
                          <input type="file" name="prodSuppImages[]" value="" class="form-control" multiple>
                        </div>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
              <div class="row">
                  <div class="col-xs-1">
                    <input type="submit" name="submit" value="ADD" class="btn">
                  </div>
              </div>
            </div>
          </form>
          <div class="modal fade" id="sizeModal" tabindex="-1">
            <div class="modal-dialog modal-lg">
              <div class="modal-content">
                <div class="col-xs-12">
                  <div class="modal-header">
                    <h4 class="modal-title">Size &amp; Qty</h4>
                    <button class="btn close" data-dismiss="modal">&times;</button>
                  </div>
                </div>
                <div class="modal-body">
                  <form class="form" action="index.html" method="post" autofill="off">
                    <?php
                      if(isset($_POST['ctgSgt'])):
                        //for filling size modal
                        $optionsArr = array('$clothingTop' => 'clothing' , '$clothingKids'=>'clothing');
                          $ediblesArr = array('fruits','vegetables','snacks','beverages');
                          $clothingTop = array('x-small','small','medium','large','x-large','xx-large');
                          $clothingBottom = array('28','30','32','34','36','38','40','42');
                          $clothingKids = array('8-10','11-12','13-14','15-16');
                          $footwear = array('1','2','3','4','5','6','7','8','9','10','11','12');

                        $ctg=isset($_POST['ctgSgt']);
                        if(in_array($ctg, $ediblesArr)){
                          $sizes="1kg,2kg,3kg,5kg,10kg,50g,100g,250g,1litre,2litre,3litre,5litre, 500millilitres,600millilitres";
                        }
                        foreach ($item as $size):
                    ?>
                    <div class="col-sm-4">
                      <div class="form-group">
                        <label for="size">size</label>
                        <input type="text" name="" value="<?=$size?>" id='size' class="form-control">
                      </div>
                    </div>
                    <div class="col-sm-2">
                      <div class="form-group">
                        <label for="qty">qty</label>
                        <input type="number" name="" value="" id='qty' class="form-control" min="1">
                      </div>
                    </div>
                  <?php endforeach;endif; ?>
                    <div class="col-sm-4">
                      <div class="form-group">
                        <label for="size">size</label>
                        <input type="text" name="" value="" id='size' class="form-control">
                      </div>
                    </div>
                    <div class="col-sm-2">
                      <div class="form-group">
                        <label for="qty">qty</label>
                        <input type="number" name="" value="" id='qty' class="form-control">
                      </div>
                    </div>
                    <div class="col-sm-4">
                      <div class="form-group">
                        <input type="text" name="" value="" id='size' class="form-control">
                      </div>
                    </div>
                    <div class="col-sm-2">
                      <div class="form-group">
                        <input type="number" name="" value="" id='qty' class="form-control">
                      </div>
                    </div>
                    <div class="col-sm-4">
                      <div class="form-group">
                        <input type="text" name="" value="" id='size' class="form-control">
                      </div>
                    </div>
                    <div class="col-sm-2">
                      <div class="form-group">
                        <input type="number" name="" value="" id='qty' class="form-control">
                      </div>
                    </div>
                  </form>
                  <div class="modal-footer">
                    <input type="button" name="" value="save" class="btn btn-primary">
                    <input type="reset" name="" value="cancel" class="btn btn-danger">
                  </div>
                </div>
              </div>
            </div>
          </div>
          <table class="table hidden">
            <thead>
              <th>sku</th>
              <th>name</th>
              <th>quantity</th>
            </thead>
            <tbody>
              <tr>
                <td>115623</td>
                <td>apple</td>
                <td>56</td>
              </tr>
            </tbody>
          </table>
          <!--product removal tab-->
          <form class="form hidden" action="productsWorkspace.php" method="get" id="remove" enctype="multipart/form-data" autofill="off">
            <div class="container-fluid">
              <div class="row">
                <div class="col-xs-2">
                  <div class="form-group">
                    <label for="prodRemID">ID</label>
                    <input type=text class="form-control" name="prodRemID" value="<?=isset($_GET['prodRemID'])?$_GET['prodRemID']:''?>">
                  </div>
                  <div class="form-group">
                    <label for="prodRemCateg">Category</label>
                    <input type=text list="categOptions" class="form-control" name="prodRemCateg">
                    <datalist id="categOptions">
                      <option value="11"/>
                      <option value="12"/>
                    </datalist>
                  </div>
                  <div class="form-group">
                    <label for="prodRemBrand">Brand</label>
                    <input type=text list="brandRemOptions" class="form-control" name="prodRemBrand">
                    <datalist id="brandOptions">
                      <option value="11"/>
                      <option value="12"/>
                    </datalist>
                  </div>
                  <div class="form-group">
                    <label for="prodRemRetailer">Retailer</label>
                    <input type=text list="retailerOptions" class="form-control" name="prodRemRetailer">
                    <datalist id="retailerOptions">
                      <option value="11"/>
                      <option value="12"/>
                    </datalist>
                    <input type="hidden" name="page_state" value="<?= isset($_GET['page_state'])?$_GET['page_state']:''?>">
                  </div>
                </div>
                <div class="col-xs-10">
                  <table class="table">
                    <thead>
                      <th class="col-xs-1"><input type="checkbox" id="checkall"></th>
                      <th>sku</th>
                      <th>name</th>
                    </thead>
                    <tbody>
                      <?php
                      include("../includes/connect.php");
                        if(!empty($_GET['prodRemID'])||!empty($_GET['prodRemCateg'])||!empty($_GET['prodRemBrand'])||!empty($_GET['prodRemRetailer'])){
                          $id=$_GET['prodRemID'];
                          $ctg=$_GET['prodRemCateg'];
                          $brd=$_GET['prodRemBrand'];
                          $rtl=$_GET['prodRemRetailer'];
                          $q="SELECT productID, productName FROM products WHERE ";
                          if(!empty($_GET['prodRemID'])){
                            $q=$q." AND productID='$id'";

                          }
                          if(!empty($_GET['prodRemCateg'])){
                            $q=$q." AND categories='$ctg'";
                          }
                          if(!empty($_GET['prodRemBrand'])){
                            $q=$q." AND brand='$brd'";
                          }
                          if(!empty($_GET['prodRemRetailer'])){
                            $q=$q." AND retailer='$rtl'";
                          }
                          $p=strpos($q,"AND");
                          $q=substr($q,0,$p).substr($q, ($p+3));

                          $massremquery=mysqli_query($connector,$q);
                            if(mysqli_num_rows($massremquery)==1){
                              $massremresults = mysqli_fetch_assoc($massremquery);
                              echo '<tr>
                                <td class="col-xs-1"><input type="checkbox" class="checkable"></td>
                                <td>'.$massremresults['productID'].'</td>
                                <td>'.$massremresults['productName'].'</td>
                              </tr>';
                              mysqli_close($connector);
                            }elseif (mysqli_num_rows($massremquery)>1) {
                              while($massremresults = mysqli_fetch_assoc($massremquery)){
                                echo '<tr>
                                  <td class="col-xs-1"><input type="checkbox" class="checkable"></td>
                                  <td>'.$massremresults['productID'].'</td>
                                  <td>'.$massremresults['productName'].'</td>
                                </tr>';
                              }
                              mysqli_close($connector);
                            }
                        }
                      ?>
                    </tbody>
                  </table>
                </div>
              </div>
              <div class="row">
                  <div class="col-xs-1">
                    <input type="submit" name="submit" value="ADD" class="btn">
                  </div>
              </div>
            </div>
          </form>
          <!--product edit tab-->
          <form class="form hidden" action="productModification.php" method="post" id="edit" enctype="multipart/form-data" autofill="off">
            <div class="container-fluid">
              <div class="row">
                <div class="col-xs-2">

                  <div class="form-group">
                    <label for="prodID">ID</label>
                    <input type=text class="form-control" name="prodID">
                  </div>
                  <div class="form-group">
                    <label for="prodCateg">Category</label>
                    <input type=text list="categOptions" class="form-control suggestions" name="prodCateg">
                    <datalist id="categOptions">
                    </datalist>
                  </div>
                  <div class="form-group">
                    <label for="prodBrand">Brand</label>
                    <input type=text list="brandOptions" class="form-control" name="prodBrand">
                    <datalist id="brandOptions">
                      <option value="11"/>
                      <option value="12"/>
                    </datalist>
                  </div>
                  <div class="form-group">
                    <label for="prodRetailer">Retailer</label>
                    <input type=text list="retailerOptions" class="form-control" name="prodRetailer" id="editRetailer">
                    <datalist id="retailerOptions">
                      <option value="11"/>
                      <option value="12"/>
                    </datalist>
                  </div>
                </div>
                <div class="col-xs-10">
                  <div class="form-group">
                    <label for="prodName">Name</label>
                    <input type="text" name="prodName" value="" class="form-control" id="prodName">
                  </div>
                  <div class="container-fluid">
                    <div class="row">
                      <div class="form-group col-sm-6">
                        <label for="prodPrice">Price</label>
                        <input type="text" name="prodPrice" value="" class="form-control" id="prodPrice">
                      </div>
                      <div class="form-group col-sm-6">
                        <label for="prodQty">Size & Quantity</label>
                        <input type="text" name="prodQty" value="" class="form-control" readonly>
                      </div>
                    </div>
                  </div>
                  <div class="form-group">
                    <label for="prodDescr">Description</label>
                    <textarea name="prodDescr" class="form-control" id="editDescr"></textarea>
                  </div>
                  <div class="container-fluid">
                    <div class="row">
                      <div class="col-sm-6">
                        <div class="">
                          <div class="row">
                          <div class="col-md-6">Main Image</div>
                          <div class="col-md-2 col-md-offset-4">
                            <span class="glyphicon glyphicon-remove" id="edit-m-img-remove-icon"></span>
                          </div>
                          </div></div>
                        <div class="" id="editMainImg">
                          <img src="" >
                        </div>
                        <div class="form-group hidden" id="edit-m-img-input">
                          <input type="file" name="prodMainImage" value="" class="form-control">

                        </div>
                      </div>
                      <div class="col-sm-6">
                        <div class="row">
                        <div class="col-md-6">Supporting Images</div>
                        <div class="col-md-2 col-md-offset-4">
                          <span class="glyphicon glyphicon-remove" id="edit-supp-img-remove-icon"></span>
                        </div>
                        </div>
                        <!--<div class="form-group">
                          <label for="prodSuppImages">Supporting Images</label>
                          <input type="file" name="prodSuppImages[]" value="" class="form-control" multiple>
                        </div>-->
                        <div id="supp-img-carousel" class="carousel slide" data-ride="carousel" interval="false" data-wrap="true">
                          <div class="carousel-inner">
                            <div class="carousel-item active">
                              <img src="../images/backpack-grey-whie-dotted.jpg" alt="">
                            </div>
                            <div class="carousel-item">
                              <img src="../images/cellphone-smartwatch.jpg" alt="">
                            </div>
                          </div>
                          <div>
                              <a href="supp-img-carousel" class="carousel-control-prev hidden" data-slide="prev">
                                  <span class="carousel-control-prev-icon"></span>
                              </a>
                              <a href="#supp-img-carousel" class="carousel-control-next" data-slide="next">
                                  <span class="carousel-control-next-icon"></span>
                              </a>
                          </div>
                        </div>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
              <div class="row">
                  <div class="col-xs-1">
                    <input type="submit" name="submit" value="Save" class="btn">
                  </div>
              </div>
            </div>
          </form>
      </div>
    </div>
  </main>
    <?php include_once("../includes/scriptTags.php"); ?>

    <!--controls window switch-->
    <script type="text/javascript">
      function toggleActionWindow(id){
        var current;
        var pressed;
          var tabs = new Array('view', 'add', 'remove', 'edit');
          for(i=0;i<tabs.length;i++){
            if(document.getElementById(tabs[i]).classList.contains("visible")){
              current=i+1;
            }
          }
          for(i=0;i<tabs.length;i++){
            if(id == tabs[i]){
              pressed=i+1;
              $('#t0'+current).css('border-bottom',"1px solid #ccc");
              $('#t0'+pressed).css('border-bottom',"2px solid blue");

              document.getElementById(tabs[i]).classList.replace("hidden","visible");
              $('input[name="page_state"]').val(tabs[i]+"-"+pressed);
            }else{
              document.getElementById(tabs[i]).classList.replace("visible","hidden");
            }
          }
      }

      if($('input[name="page_state"]').val()!=''){
        page_stateArr=($('input[name="page_state"]').val()).split("-");
        $("#view").addClass("hidden").removeClass("visible");
        $('#t01').css('border-bottom',"1px solid #ccc");
        $('#'+page_stateArr[0]).addClass("visible").removeClass("hidden");
        $('#t0'+page_stateArr[1]).css('border-bottom',"2px solid blue");

      }else{
        $("#view").addClass("visible").removeClass("hidden");
        $('#t01').css('border-bottom',"2px solid blue");
      }
    </script>
    <!--autofill functionality-->
    <script type="text/javascript">
    function setIdAdd(id){
      $.post("productsAuto.php", {editID: id}, function(data, status){
        results=data.split("^");
        suppRes=results[7].split(";");
        $('#prodName').val(results[0]);
        $('#editCateg').val(results[1]);
        $('#prodPrice').val(results[2]);
        $('#editBrand').val(results[3]);
        $('#editDescr').val(results[4]);
        $('#editRetailer').val(results[5]);
        $('#editMainImg').attr("src",results[6]);
        $('#editSuppImg').attr("src",suppRes[0]);
      });
    }
    </script>
    <!--calls edit button and tab switch on edit-->
    <script type="text/javascript">
    function editProd(id){
      setIdAdd(id);
      toggleActionWindow("edit");
    }
    </script>
    <!--handles quick removal of products-->
    <script type="text/javascript">
    function setIdRem(id){
      $.post("productsAuto.php", {remID: id}, function(data, status){
        //document.getElementById(id).classList.add("hidden");
        $(id).remove();
        //$('#prodName').val(results[0]);
      });
    }
    </script>
    <!--for suggesting list of options in form-->
    <script type="text/javascript">
      $('.suggestions').click(function(){
        var sgtBox=$(this).attr('list');
        sgtArr= sgtBox.split("Options");
        //alert(sgtArr[0]);
        $.post('productsAuto.php',{listOpt:sgtArr[0]}, function(data, status){
          resArr=data.split("^");
          //alert(resArr[0]);
          $('#'+sgtBox).html(resArr);
          //$('#categOptions').html(resArr[1]);
          //alert('done');
        });
      });
    </script>
    <!--autofills form when id is given-->
    <script type="text/javascript">
      $('#prodID').keyup(function(){
        request = $(this).val();
        $.post('productsAuto.php', {request: request}, function(data, status){
          if(data !=''){
            var product_info = data.split('/');
            $("#addPname").val(product_info[0]);
            $("#addPprice").val(product_info[1]);
            //$("#addPqty").val(product_info[2]);
            $("#addPbrand").val(product_info[2]);
            $("#addPdescr").val(product_info[3]);
            $("#addPretailer").val(product_info[4]);
          }
        });
      });
    </script>
    <!--checks all items-->
    <script type="text/javascript">
      $(document).ready(function(){
        $('#checkall').click(function(){
          $('.checkable').prop("checked",$(this).prop("checked"));
        });
      });
    </script>
    <!--quick view of product info-->
    <script type="text/javascript">
      $('#view tbody tr').click(function(){
        if($(this).next(".details_prmt").length==0){
          $(this).after('<tr class="details_prmt"><td colspan=5><div class="container-fluid"><div class="row"><div class="col-sm-4"><img src="../images/apple.jpg" alt="" class="img-fluid"></div><div class="col-sm-8"></div></div></div></td></tr>');

        }else{
          $(this).next(".details_prmt").toggleClass('hidden');
        }

      });
    </script>
    <!--fills size list if category is set-->
    <script type="text/javascript">
      $('#add input[name="prodCateg"]').keyup(function(){
        if($(this).val().length>4){
          $.post("productsAuto.php",{ctgSgt:$(this).val()},function(data, status){
            alert(data+" s:"+status);
          });
        }

      });
    </script>
    <script type="text/javascript">
      $('#edit-m-img-remove-icon').click(function(){
        $('#edit-m-img-input').addClass('visible').removeClass('hidden');
        $('#editMainImg').addClass('hidden').removeClass('visible');
      });
    </script>
    <script type="text/javascript">
      $('#edit-supp-img-remove-icon').click(function(){
        $('#supp-img-carousel .active').remove();
        //$('#supp-img-carousel .active').next('.carousel-item').addClass('active');

        $('#supp-img-carousel .carousel-inner').children('.carousel-item').addClass('active');
      });
    </script>
  </body>
</html>
