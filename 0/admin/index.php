
<html lang="en" dir="ltr">
  <head>
    <?php include_once("../includes/headTags.php"); ?>
    <title>Admin Home</title>
    <style media="screen">
      .xssf{
        min-height: 200px;
        background-color: silver;
        margin:10px 10px;
      }
      div.row{
        padding-left: 14%;
      }
      a:hover{
        text-decoration: none;
      }
      a{
        color: grey;
      }
    </style>
  </head>
  <body>
    <?php include_once("includes/header.php") ?>
    <main class="container-fluid"  style="padding-top:70px;">
      <div class="row text-center">
        <div class="col-sm-5 col-md-3 xssf">
          <h3>Departments</h3>
          <hr>
          <ul class="list-unstyled">
            <li><a href="departmentsWorkspace.php">View Departments</a></li>
            <li><a href="">Remove Departments</a></li>
            <li><a href="">Add Departments</a></li>
            <li><a href="">Edit Departments</a></li>
          </ul>
        </div>
        <div class="col-sm-5 col-md-3 xssf">
          <h3>Products</h3>
        </div>
        <div class="col-sm-5 col-md-3 xssf">
          <h3>Retailers</h3>
        </div>
        <div class="col-sm-5 col-md-3 xssf">
          <h3>Users</h3>
        </div>
        <div class="col-sm-5 col-md-3 xssf">
          <h3>Orders</h3>
        </div>
        <div class="col-sm-5 col-md-3 xssf">
          <h3>Sales</h3>
        </div>
      </div>
    </main>
    <?php include_once("../includes/scriptTags.php"); ?>
  </body>
</html>
