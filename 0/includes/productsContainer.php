
<div class="" style="margin-bottom:30px; position: relative;">
       <h4>Carousel Name</h4>
     <h5>SubHeading</h5>
     <div class="">
       <div id="produce" class="products-slide">
         <div class="product-slide-inner">
           <?php
           $sql = "SELECT productID, productName, mainImage FROM products WHERE categories ='fruit'";
            $query=mysqli_query($connector,$sql);
            while($query_results=mysqli_fetch_assoc($query)):
           ?>
           <div class="slide-item active">
               <div class="main-slide-content">
                 <div class="product-header">
                   <div class="product-name"><?php echo $query_results['productName'] ?></div>
                   <div class="additional-info">
                     <span class="glyphicon glyphicon-option-vertical additional-info-icon"></span>
                   </div>
                 </div>
                 <div class="product-box">
                   <a href="../pages/productPage.php?req_id=<?php echo $query_results['productID'] ?>">
                         <img src="../Images/<?php echo $query_results['mainImage'] ?>" style="width:100%; height: 90%;"/>
                   </a>
                 </div>
               </div>
           </div>
         <?php endwhile; ?>
         </div>
         <div class="slide-indicators">
            <a href="#Carousel" class="slide-control-prev slide-control" data-slide="prev">
                <span>&#10094;</span>
            </a>
            <a href="#Carousel" class="slide-control-next slide-control" data-slide="next">
                <span>&#10095;</span>
            </a>
          </div>
       </div>
     </div>
 </div>
 

