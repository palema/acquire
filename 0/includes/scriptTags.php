<?php $abspath='\0\\' ?>
<script src="<?= $abspath ?>bootstrap/assets/js/vendor/jquery-slim.min.js"></script>
<script src="<?= $abspath ?>bootstrap/assets/js/vendor/popper.min.js"></script>
<script src="<?= $abspath ?>bootstrap/dist/js/bootstrap.min.js"></script>
<script src="<?= $abspath ?>js_files/userMenu.js"></script>
<script src="http://ajax.googleapis.com/ajax/libs/jquery/1.11.2/jquery.min.js"></script>
<script src="https://maps.googleapis.com/maps/api/js?key=YOUR_API_KEY&callback=initMap" async defer>

</script>
<script>
  function initMap() {
    var myLatLng = {lat: -25.363, lng: 131.044};

    // Create a map object and specify the DOM element
    // for display.
    var map = new google.maps.Map(document.getElementById('map'), {
      center: myLatLng,
      zoom: 4
    });

    // Create a marker and set its position.
    var marker = new google.maps.Marker({
      map: map,
      position: myLatLng,
      title: 'Hello World!'
    });
  }

</script>

<script type="text/javascript">
    $(document).ready(function(){
      $('#searchSubmitBtn').click(function(){
        if($('#searchBar').val()==''){
          $('#searchSubmitBtn').attr('type', 'button');
          $('#searchOutput').css('height','0px');
        }else{
          $('#searchSubmitBtn').attr('type', 'submit');
        }
      });
      $('#searchBar').keyup(function(){
        if($('#searchBar').val()==''){
          $('#searchSubmitBtn').attr('type', 'button');
          $('#searchOutput').addClass('hidden');
        }else{
          $('#searchOutput').addClass('visible').removeClass('hidden');
          $('#searchSubmitBtn').attr('type', 'submit');
        }
        var query = $(this).val();
        if(query != ''){
          $.ajax({
            url:"../../asyncSearchHandler.php",
            method: "POST",
            data: {query:query},
            success:function(data){
              $('#searchOutput').fadeIn();
              $('#searchOutput').html(data);
            }
          });
        }
      });
    });

  $(document).on('click','.searchResults',function(){
    if($(this).text()!="No results"){
      $('#searchBar').val($(this).text());
      $('#searchOutput').fadeOut();
    }
  });
</script>

<!-- sidebar slide and size handler  -->
<script>
  function sidebart(){
      if($('#sidebar').hasClass('actives')){
        var window_size = $('body').css('width').split('px')[0];
        if(window_size<540){
          $('aside').css('width','100vw');
          $('body').css('overflow-y','hidden');
        }
        $('#sidebar').css('right','0px');
        document.getElementById('sidebar').classList.remove('actives');
      }else{
        $('body').css('overflow-y','initial');
        $('#sidebar').css('right','-'+$('#sidebar').css('width').split('px')[0]+'px');
        $('#sidebar').addClass('actives');
      }
      //alert($('body').css('width').split('px')[0]);
  }
</script>
<!--checks window size before load-->
<script type="text/javascript">
  $('.carousel-item').each(function(){
    if($(this).height()<158){
      var max_items=$(this).width()/176.667;
      var current_items=$('.product-container').length;
      if(current_items>max_items){
        //$('.second-container').prepend($('.product-container').last());
        $(this).find('.second-container').prepend($('.product-container').last());
      }
    }
  });
</script>
<!--hides search dropdown-->
<script type="text/javascript">
  $(window).click(function(e){
    if(!e.target.matches('#searchOutput')){
      if($('#searchOutput').hasClass('visible')){
        $('#searchOutput').removeClass('visible').addClass('hidden');
      }
    }
  });
</script>
<!--parallax effect for sidebar and navigation  -->
<!-- <script type="text/javascript">
var previous_scroll_location=0;
    var nav_height=$('#nav-wrapper').css('height').split('px')[0];;
  $(window).scroll(function(){
    var nav_wrapper_height=$('#nav-wrapper').css('height').split('px')[0];
    if(nav_wrapper_height!=77){
      var vscroll=$(this).scrollTop();
      var curr_height = $('#nav-wrapper').css('height').split('px')[0];
      if(vscroll<60){
        //translates logo
        $('#logo').css({
          'transform':'translate(0px,-'+vscroll*1.2+'px)', 'opacity':20/vscroll
        });
        //translates search form
        $('#search-form-wrapper').css({
          'transform':'translate(0px,-'+vscroll*1.1+'px)'
        });
        //translates navigation box
        if(previous_scroll_location>vscroll){
          $('#nav-wrapper').css('height',(curr_height+vscroll)+'px');
        }else{
          $('#nav-wrapper').css('height',(curr_height-vscroll)+'px');
        }
      }
      nav_height=$('#nav-wrapper').css('height').split('px')[0];
      $('#navbarBreadcrump').css('margin-top',nav_height+'px');
      previous_scroll_location=vscroll;
    }

    $('#navbarBreadcrump').css({'transform':'translate(0px, -30px)','transition':"all, .8s linear"});
    $('#m_nav').css({'transform':'translate(0px, 30px)','transition':"all, .8s linear"});
    //$('#s_nav').css({'transform':'translate(0px, -25px)','transition':"all, 1.5s linear"});

  });
if(nav_height==131){
    $('#navbarBreadcrump').css('margin-top',nav_height+'px');
    $('main').css('margin-top','160px');
    $('aside').addClass('sidebar-sm-dev');
}

</script> -->
