<div class="container-fluid" style="padding: 0px 0px;">
  <div class="container-fluid" id="sidebar-main">
    <div class=" active-window" id="map-locator">
          <div class="row" id="sidebar-map">
              <div class="col-xs-12">
                    <div style="height:200px;width:100%;" id="map"></div>

              </div>
          </div>
          <hr>
          <br>
          <div class="row">
              <div class="col-sm-12">
                <div class="input-group">
                    <div class="input-group-addon">
                        <div class="dropdown">
                            <a href="#" class="dropdown-toggle" data-toggle="dropdown">All</a>
                            <div class="dropdown-menu">
                                <a href="#" class="dropdown-item">Retailers</a>
                                <a href="#" class="dropdown-item">Brands</a>
                            </div>
                        </div>
                    </div>
                    <input type="text" placeholder="search for retailers" class="form-control">
                </div>
              </div>
          </div>
        </div>
    <div class="inactive-window" id="user-account">
      <div class="row" style="padding-left:10px;">
        <div class="btn-group">
          <span class="btn btn-default" onclick="switchWindows('userProfileWindow')">Profile</span>
          <span class="btn btn-default" onclick="switchWindows('userBalanceWindow')">Finance</span>
          <span class="btn btn-default" onclick="switchWindows('userBalanceWindow')">Wardrobe</span>
          <span class="btn btn-default" onclick="switchWindows('userBalanceWindow')">Pinned</span>
        </div>
      </div>
      <div>
            <div class="panel user-account-panels visible" id="userProfileWindow" >
                <fieldset style="border:solid 2px black">
                  <legend>Bio</legend>
                  <ul class="list-group">
                      <li class="list-group-item" id="userProfileUsername">
                        Username
                        <?php
                          if(isset($_SESSION['username'])){
                            echo $_SESSION['username'];
                          }else{
                            echo "";
                          }
                        ?>
                      </li>
                      <li class="list-group-item">
                          Firstname
                      </li>
                      <li class="list-group-item">Lastname</li>
                      <li class="list-group-item">Password</li>
                      <li class="list-group-item">Age</li>
                      <li class="list-group-item">Gender</li>
                      <li class="list-group-item">Height</li>
                  </ul>
                </fieldset>
                <fieldset>
                  <legend>Preferences</legend>

                </fieldset>
                <fieldset>
                  <legend>Blacklist</legend>
                </fieldset>
                <fieldset>
                  <legend>Whitelist</legend>
                </fieldset>
            </div>
            <div class="panel  hidden user-account-panels" id='userBalanceWindow'>
              <div class="well">
                  <div class="row">
                      <div class="col-sm-8">Available Balance</div>
                      <div class="col-sm-4">N$ 400</div>
                  </div>
              </div>
              <div class="well">
                <div class="row">
                  <ul class="nav nav-tabs nav-tabs-justified">
                    <li><a href="#">Transfer</a></li>
                    <li><a href="#">Deposit</a></li>
                  </ul>
                </div>
              </div>
            </div>
          </div>
    </div>
  </div>
</div>
