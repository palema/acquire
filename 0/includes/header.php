<?php
include("connect.php");
session_start();
$sql="SELECT departmentName,departmentCode FROM departments WHERE parent='0'";
$query=mysqli_query($connector,$sql);
$depCode;
$subDepCode;
$child='';
?>
    <div class="navbar navbar-light bg-light navbar-fixed-top" id="nav-wrapper">

        <div class="col-md-12 navbar-expand-sm" style="padding:0px;">
            <div class="container-fluid">
              <div class="row">
                <div class="col-sm-12">
                  <div class="navbar-header" style="width:100%;">
                    <div class="container-fluid">
                      <div class="row">
                        <div class="col-sm-4 col-xs-4 col-sm-offset-0 col-md-1" style="padding:0px;">
                          <a href="../pages/home.php"><span class="navbar-brand" id="logo">acquire</span></a>                 
                        </div>
                        <div class="col-xs-8 col-md-7 hidden-xs" id="search-form-wrapper" style="padding-right:0px;z-index:950;">
                          <form action="../productPage/syncSearchHandler.php" method="post" class="navbar-brand navbar-form form-horizontal" style="width:100%;" role="search">
                              <div class="row">
                                  <div class="form-group col-sm-12">
                                      <div class="input-group">
                                          <div class="input-group-addon hidden-md hidden">
                                              <div class="dropdown">
                                                  <a href="#" class="dropdown-toggle" data-toggle="dropdown">All</a>
                                                  <div class="dropdown-menu">
                                                      <a href="#" class="dropdown-item">Products</a>
                                                      <a href="#" class="dropdown-item">Retailers</a>
                                                      <a href="#" class="dropdown-item">Brands</a>
                                                  </div>
                                              </div>
                                          </div>
                                          <input type="text" class="form-control" placeholder="search" name="query" id="searchBar">
                                          <span class="input-group-append hidden-xs">
                                          <button type="submit" class="btn btn-default" id="searchSubmitBtn">
                                              <span class="glyphicon glyphicon-search"></span>
                                          </button>
                                      </span>
                                      </div>
                                  </div>
                              </div>
                          </form>
                          <div class=" hidden" id="searchOutput"></div>
                        </div>
                        <div class="col-xs-8 col-md-4" style="padding:0px;">
                            <div class="pull-right">
								<ul class="nav  navigation-right">
								  <li class="nav-item visible-xs nav-right-item" id="side-btn">
									<a class="nav-link" style="border-radius:5px; padding: 15px 5px;">
										<span class="glyphicon glyphicon-search"></span>
									</a> 
								  </li>
								  <li class="nav-item nav-right-item">
									<a href="#map-locator" class="nav-link" style="padding: 15px 5px;">
										<span class="glyphicon glyphicon-map-marker"></span>
									</a>
								  </li>
								  <li class="nav-item nav-right-item">
									<a href="#shopping-cart" class="nav-link" style="padding: 15px 5px;">
										<span class="glyphicon glyphicon-shopping-cart"></span>
									</a>
								  </li>
								  <li class="nav-item nav-right-item">
									<a href="<?php echo isset($_SESSION['username'])?'#user-account':'#loginModalMenu'; ?>" class="userIconLink nav-link" style="padding: 15px 5px;">
										<span class="glyphicon glyphicon-user"></span>
									</a>
								  </li>
								  <li class="nav-item nav-right-item" id="side-btn">
									<a class="nav-link" onclick="sidebart()" style="border-radius:5px; padding: 15px 5px;">
										<span class="glyphicon glyphicon-menu-hamburger"></span>
									</a> 
								  </li>
								</ul>
                            </div>
                        </div>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
            </div>
        </div>
		<ul class="breadcrumb" style="padding-left: 138px;">
		  <li id="departments" class="breadcrumb-item">Departments</li>
		  <li id="retailers" class="breadcrumb-item">Retailers</li>
		  <li id="brands" class="breadcrumb-item">Brands</li>
		</ul>
		
		<ul id="navigation-dropdown" class="hidden" style="list-style: none; padding: 2px 2px; position: absolute; top:110px; left: 145px; background-color: white; width: 240px; box-shadow: 1px 0px 2px blue;">
			<li><a href="groceries.php?depQuery=groc">Groceries</a></li>
			<li><a href="depProducts.php?depQuery=groc">Produce</a></li>
			<li><a href="#">Men's Fashion</a></li>
			<li><a href="#">See all departments</a>
		</ul>
    </div>
    <div class="modal" id="loginModalMenu" tabindex="-1">
        <div class="modal-dialog" style="margin-top:100px;">
            <div class="modal-content">
                <div class="modal-header">
                    <h4 class="modal-title">Login</h4>
                    <button class="btn close" data-dismiss="modal">&times;</button>
                </div>
                <div class="modal-body">
                    <form action="\0\userLogin.php" method="post">
                        <div class="form-group">
                            <div class="input-group">
                                <label for="username" class="control-label">Username</label>
                                <input type="text" id="username" placeholder="Enter username" class="form-control" name="username">
                            </div>
                            <div class="input-group">
                                <label for="password" class="control-label">Password</label>
                                <input type="password" id="password" placeholder="Enter password" class="form-control" name="password">
                            </div>
                        </div>
                        <div class="modal-footer">
                          <span><a href="\0\pages\userRegistrationForm.php">Sign Up</a></span>
                            <button type="submit" class="btn">Login</button>
                        </div>
                    </form>
                </div>

            </div>
        </div>
    </div>
	
</header>

