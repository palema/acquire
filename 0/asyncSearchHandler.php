<?php
include('includes\connect.php');
if(isset($_POST["query"])):
  $searchQuery=mysqli_real_escape_string($connector,$_POST['query']);
  $sql="SELECT * FROM products WHERE productName LIKE '%".$searchQuery."%'";
  $results=mysqli_query($connector,$sql);
  $output='';
  $output='<ul class="list-unstyled">';
  if(mysqli_num_rows( $results)>0):
    while($row = mysqli_fetch_array($results)):
      $output .= '<li class="searchResults">'.$row["productName"].'</li>';
    endwhile;
    else:
      $output .= '<li>No results</li>';
    endif;
    $output .= '</ul>';
    echo $output;
endif;
?>
