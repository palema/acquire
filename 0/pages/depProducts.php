<?php
require '../includes/connect.php';
if(isset($_GET['depQuery'])):
$department=$_GET['depQuery'];
$product_sql="SELECT * FROM products WHERE categories='$department'";
$sql="SELECT * FROM departments WHERE parent='$department'";
$mass_dep_results=mysqli_query($connector,$sql);
$dep_tmp;
while($mass_dep=mysqli_fetch_assoc($mass_dep_results)){
  $dep_tmp=$mass_dep['departmentCode'];
  $product_sql=$product_sql." OR categories=\"".$dep_tmp."\"";
}

$mass_products=mysqli_query($connector,$product_sql);
?>
<!DOCTYPE html>
<html lang="en" dir="ltr">
  <head>
    <?php include_once("../includes/headTags.php"); ?>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <link rel="stylesheet" href="bootstrap/dist/css/bootstrap.min.css">
    <link rel="stylesheet" href="bootstraps/css/bootstrap.min.css">
    <link rel="stylesheet" href="/maps/documentation/javascript/cgc/demos.css">
	<link rel="stylesheet" href="../css_files/global_css.css">
	<link rel="stylesheet" href="../css_files/department_products_css.css">
    <title>Department | products</title>
  </head>
  <body>
    <div class="container-fluid">
      <div class="row">
        <div>
          <?php include('../includes/header.php'); ?>
		  <div style="height: 200px; font-size:40px; background-color:silver; position: absolute; top: 80px; width: 100vw;">
		  <div style="padding-left: 40px;margin-top: 30px;">Produce</div>
		  <div class="" style="height:100px; position: absolute; bottom: 0px;white-space: nowrap; overflow:hidden;">
              <div class="hexagon"></div>
              <div class="hexagon"></div>
              <div class="hexagon"></div>
              <div class="hexagon"></div>
            </div>
		  </div>
          <div class="row">
            <main class="col-xs-12 col-md-12">
              <div class="container-fluid">
                <div class="row">
                  <div class="col-sm-12" id="main-content">
                    <?php
                      if(mysqli_num_rows( $mass_products)>0):
                        $separator=0;
                    ?>
                    <ul class="row list-unstyled list-inline products-table">
                      <?php
                        while($row = mysqli_fetch_array($mass_products)):
                          $separator++;
                          $price=explode(".",$row['grossPrice']);
                      ?>
                      <li class="col-xs-6 col-sm-4 col-md-3 col-lg-2">
                          <img src="../images/<?=$row['mainImage']?>" alt="" class="img-responsive">
                      </li>
                    <?php
                      if($separator==12){
                        $separator=0;
                        echo '<li class="col-xs-12 separator"></li>';
                      }
                      endwhile; ?>
                    </ul>
                    <?php endif; ?>
                  </div>
                </div>
              </div>
            </main>
            <aside class="bg-light actives" id="sidebar">
              <?php include_once("../includes/sidebar.php") ?>
            </aside>
          </div>
        </div>
      </div>
      <div class="row">
        <div class="col-xs-12" style="padding:0px;">
          <?php //include_once("../includes/footer.php") ?>
        </div>
      </div>
    </div>
    <?php include_once("../includes/scriptTags.php") ?>
	<script src="../js_files/global_javascript.js"></script>
	<script src="../js_files/userMenu.js"></script>
  </body>
</html>
<?php endif; ?>
