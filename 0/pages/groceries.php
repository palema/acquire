<!DOCTYPE html>
<?php
require '../includes/connect.php';

?>
<html>
<head>
  <?php include_once("../includes/headTags.php") ?>
  <link rel="stylesheet" href="../css_files/global_css.css">
  <link rel="stylesheet" href="../css_files/department_products_css.css">
  <title>Department | <?php $pageTitle='Groceries'; echo $pageTitle; ?></title>
</head>
<body>
          <?php include("../includes/header.php") ?>
            <!-- end of top part -->
			<div style="height: 200px; font-size:40px; background-color:silver; position: absolute; top: 80px; width: 100vw;">
			  <div style="padding-left: 40px;margin-top: 30px;">Groceries</div>
			  <div class="" style="height:100px; position: absolute; bottom: 0px;white-space: nowrap; overflow:hidden;">
				  <div class="hexagon"></div>
				  <div class="hexagon"></div>
				  <div class="hexagon"></div>
				  <div class="hexagon"></div>
				</div>
			</div>
          <main class="">
			<?php include("../includes/productsContainer.php") ?>
            <div class="normal-single-display" style="height:250px; background-color:orange;">
              <div class="col-xs-12 col-sm-6 col-md-6" style="height: 100%; padding:0px; background-color:blue; display:inline-block;">
                <div class="col-xs-8" style="display:inline-block; height: inherit;">

                </div>
                <div class="col-xs-4" style="display:inline-block; height: inherit;background-color: yellow;">
                  <div class="">Type of advert <span>store</span></div><br><br>
                  <div class="">Description/Name</div><br><br>
                  <div class=""><span>Was</span><span>Now</span></div><br><br>
                  <div class=""><button type="button" class="btn add-to-cart-btn" name="button">Add to cart</button> </div>
                </div>
              </div>
              <div class="visible-sm visible-md visible-lg col-sm-6 col-md-6" style="height: 100%; background-color:red; display:inline-block;">
                <div class="col-xs-8" style="display:inline-block; height: inherit;">

                </div>
                <div class="col-xs-4" style="display:inline-block; height: inherit;background-color: yellow;">
                  <div class="">Type of advert <span>store</span></div><br><br>
                  <div class="">Description/Name</div><br><br>
                  <div class=""><span>Was</span><span>Now</span></div><br><br>
                  <div class=""><button type="button" class="btn add-to-cart-btn" name="button">Add to cart</button> </div>
                </div>
              </div>
            </div>
            <br><br><br>
            <div class="static-banner" style="height:239px; background-color:orange;">
            </div>
            
          </main>
          <aside class="bg-light" id="sidebar">
            <?php include_once("../includes/sidebar.php") ?>
          </aside>

    <div class="row" style="box-shadow:-1px 0px 6px;">
      <div class="col-xs-12" style="padding:0px;">
        <?php //include_once("../includes/footer.php") ?>
      </div>
    </div>
<?php include_once("../includes/scriptTags.php") ?>
<script src="../js_files/global_javascript.js"></script>
</body>
</html>
