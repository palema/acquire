<html>
<head>
<link rel="stylesheet" href="\0\bootstrap/dist/css/bootstrap.min.css">
<link rel="stylesheet" href="\0\bootstraps/css/bootstrap.min.css">
<title>Sign Up</title>
</head>
<body>
<div class="container-fluid" style="margin-top:100px;">
  <div class="row">
    <div class="col-sm-4 col-sm-offset-4" style="border:solid 2px blue">
      <form action="\0\userRegistration.php" method="post" >
          <div class="form-group">
            <div class="input-group">
                <label for="username" class="control-label">Username</label>
                <input type="text" id="username" class="form-control" name="username" required>
            </div>
            <div class="input-group">
                <label for="email" class="control-label">email</label>
                <input type="email" id="email" placeholder="Enter password" class="form-control" name="email" required>
            </div>
            <div class="input-group">
                <label for="password" class="control-label">Password</label>
                <input type="password" id="password" class="form-control" name="password" required>
            </div>
            <div class="input-group">
                <label for="confirmPassword" class="control-label">Confirm Password</label>
                <input type="password" id="confirmPassword" class="form-control" name="confirmPassword" required>
            </div>
            <div class="input-group hidden">
                <label for="firstName" class="control-label">First Name</label>
                <input type="text" id="firstName" placeholder="Enter username" class="form-control" name="firstname">
            </div>
            <div class="input-group hidden">
                <label for="lastName" class="control-label">Last Name</label>
                <input type="text" id="lastName" class="form-control" name="lastname">
            </div>
            <div class="input-group hidden">
                <label for="dateOfBirth" class="control-label">Date of Birth</label>
                <input type="date" id="dateOfBirth" placeholder="Enter password" class="form-control" name="dateOfBirth">
            </div>
            <div class="input-group hidden">
                <label for="contactInfo" class="control-label">Contact</label>
                <input type="tel" id="contactInfo" placeholder="Enter mobile number" class="form-control" name="contact">
                <input type="hidden" value="<?= $_SERVER['HTTP_REFERER']; ?>" name="refPage">
            </div>
          </div>

      <button type="submit" class="btn">Next</button>

      </form>
    </div>
  </div>
</div>
<script src="\0\bootstrap/assets/js/vendor/jquery-slim.min.js"></script>
<script src="\0\bootstrap/assets/js/vendor/popper.min.js"></script>
<script src="\0\bootstrap/dist/js/bootstrap.min.js"></script>
</body>
</html>
