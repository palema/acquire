<?php
require '\0\includes\connect.php';

?>
<!DOCTYPE html>
<html lang="en" dir="ltr">
  <head>
    <?php include_once("\0\includes\headTags.php") ?>
    <style media="screen">
    .sr-product{
        margin-bottom:30px;
    }
    .sr-product img{
      border: solid 2px white;
      height: 100%;
    }
    .sr-product img:hover{
      border: solid 0px grey;
      margin-bottom: 20px;
    }
    .add-to-cart-details{
      position: absolute;
      width: 100%;
      height: 100%;
      transition: all .4s linear;
    }

    .add-to-cart-details.hiddens{
      overflow: hidden;
      width:0px;
      height: 0px;
      transition: all .4s linear;
    }

    </style>
    <title><?php $pageTitle="footwear";?>Search results</title>
  </head>
  <body>
    <div class="container-fluid">
        <div class="row">
            <div class="col-xs-12">
              <?php include("\0\includes\header.php") ?>
              <div class="row">
              <main class="col-xs-12 col-md-12">

    <?php

    if(isset($_POST["query"])):
      $output='';
      $searchQuery=mysqli_real_escape_string($connector,$_POST['query']);
      $sql="SELECT * FROM products WHERE productName LIKE '%".$searchQuery."%'";
      $results=mysqli_query($connector,$sql);
      if(mysqli_num_rows( $results)>0):
        while($row = mysqli_fetch_array($results)):
          $price=explode(".",$row['grossPrice']);
          $im=explode("../",$row['mainImage']);
    ?>
    <div class="container sr-product">
      <div class="row" style="min-height:160px;max-height:190px; overflow:hidden;">
        <div class="col-xs-2" style="padding:0px 0px;height:190px;">
          <img src="<?= "AQ/".$im[1];?>" alt="" class="img-responsive">
        </div>
        <div class="col-xs-9">
          <div class="container-fluid">
            <div class="row">
              <div class="col-xs-7">
                <div class="row">
                  <div class="col-xs-4">
                    <h4><?=$row['productName']?></h4>
                  </div>
                  <div class="col-xs-6 col-xs-offset-2">
                    <ul class="list-unstyled list-inline">
                      <li>
                        track
                      </li>
                      <li>notify</li>
                      <li>reviews</li>
                    </ul>
                  </div>
                </div>
                <div><?=$row['retailer']?></div>
                <div class="row">
                  <div class="col-xs-4">
                    <div>N$&nbsp;<?=$price[0];?><sup><?=$price[1];?></sup></div>
                  </div>
                  <div class="col-xs-3 col-xs-offset-3">
                    <span class="glyphicon glyphicon-star checked"></span>
                    <span class="glyphicon glyphicon-star checked"></span>
                    <span class="glyphicon glyphicon-star"></span>
                    <span class="glyphicon glyphicon-star"></span>
                    <span class="glyphicon glyphicon-star"></span>
                  </div>
                </div>
                <div class="">
                  <?=$row['description']?>
                </div>
              </div>
              <div class="col-xs-5" class="add-to-cart-form-wrapper">
                <form class="form-hozontal" action="index.html" method="post">
                    <div class="row">
                      <div class="col-sm-5">
                        <button type="submit" class="btn btn-primary" name="button">add to cart</button>
                      </div>
                      <div class="col-sm-1 col-xs-offset-5 hidden">
                        <span class="glyphicon glyphicon-remove"></span>
                      </div>
                    </div>
                  <div class="row hiddens add-to-cart-details">
                    <div class="col-xs-12">
                      <div class="form-group">
                        <label>Quantity</label>
                        <input type="number" name="quantity" value="" class="form-control" min="1">

                      </div>
                      <div class="form-group">
                        <div class="row">
                          <div class="col-xs-4">
                            <label for="size">Size</label>
                          </div>
                          <div class="col-xs-3 col-xs-offset-5">
                            multiple
                          </div>
                        </div>
                        <input type="number" name="size" class="form-control" value="" min="1">
                      </div>
                    </div>
                  </div>
                </form>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
    <?php
        endwhile;
      else:
        echo 'No results';
      endif;
    endif;
    ?>
      </main>
    <aside class="bg-light actives" id="sidebar">
      <?php include_once("../../sidebar.php") ?>
    </aside>

  </div>
</div>
<div class="row" style="box-shadow:-1px 0px 6px;">
  <div class="col-xs-12" style="padding:0px;">
    <?php include_once("\0\includes\footer.php") ?>
  </div>
</div>
</div>

    <?php include_once("\0\includes\scriptTags.php") ?>
    <script type="text/javascript">
        $('.sr-product').hover(function(){
          $(this).find('.add-to-cart-details').toggleClass('hiddens');
        });

    </script>
  </body>
</html>
