
<?php
require "../includes/connect.php";
$req_id=$_GET['req_id'];
$sltd_prod_sql="SELECT * FROM products WHERE productID=$req_id";
$sltd_prod_query=mysqli_query($connector,$sltd_prod_sql);
$sltd_prod_res=mysqli_fetch_assoc($sltd_prod_query);
//$suppImg=explode("",$sltd_prod_res['supportingImages']);
$price=explode(".",$sltd_prod_res['grossPrice']);
?>
<html lang="en" dir="ltr">
  <head>
    <meta charset="utf-8">
    <?php include_once("../includes/headTags.php"); ?>
    <title>Product |<?php $depCode="groc"; ?></title>
    <style media="screen">
      .jiffy{
        border-color: blue;
        margin-top: 2px;
      }
      img{
        max-width: 100%;
        height: auto;
      }

      .suggestion-box img{
        max-height: 150px;
      }
      .checked {
        color: orange;
      }
	  
	  main{
		  margin-top: 80px;
		  padding-top: 60px;

	  }
    </style>
  </head>
  <body>
    <div class="container-fluid">
        <div class="row">
            <div class="col-xs-12">
              <?php include("../includes/header.php") ?>
                <!-- end of top part -->
                <div class="row">
                <main class="col-xs-12 col-md-12">
                  <div class="container">
                    <div class="row">
                      <!--alternative view angles of product-->
                      <div class="col-sm-1 jiffy">
                        <div class="hidden-xs col-sm-12">
                          <img src="../images/greaterthanup.png" alt="" id="avopsu">
                        </div>
                        <div class="col-sm-12" style="padding:0px;">
                          <div class="container-fluid" style="min-width:60px;padding:0px;">
                              <div class="col-sm-12 col-xs-3" style="padding:0px;height:75px;" id="avop1">
                                <img src="../images/bananas2nd.jpg" alt="">
                              </div>
                              <div class="col-sm-12 col-xs-3" style="padding:0px;height:75px;" id="avop2">
                                <img src="../images/bananas2nd.jpg" alt="">
                              </div>
                              <div class="col-sm-12 col-xs-3" style="padding:0px; height:75px;" id="avop3">
                                <img src="../images/bananas2nd.jpg" alt="">
                              </div>
                              <div class="col-sm-12 col-xs-3" style="padding:0px;height:75px;" id="avop4">
                                <img src="../images/bananas2nd.jpg" alt="">
                              </div>
                          </div>
                        </div>
                        <div class="hidden-xs col-sm-12">
                              <img src="../images/greaterthandown.png" alt="" id="avopsd">
                        </div>
                      </div>
                      <div class="col-sm-11 col-xs-12" style="padding:0px;">
                        <div class="container-fluid">
                          <div class="row">
                            <div class="col-xs-12 col-sm-5 jiffy" style="padding:0px;max-height:368.8px;">
                              <img src="../images/<?=$sltd_prod_res['mainImage'];?>" alt="<?=$sltd_prod_res['mainImage'];?>" class="img-fluid">
                            </div>
                            <div class="col-xs-12 col-sm-7 jiffy" style="padding:0px;">
                              <div class="container-fluid">
                                <div class="row">
                                  <div class="btn-group">
                                    <span class="btn btn-default" onclick="salesWindow('product')">Product</span>
                                    <span class="btn btn-default" onclick="salesWindow('description')">Description</span>
                                    <span class="btn btn-default"onclick="salesWindow('reviews')">Reviews</span>
                                  </div>
                                </div>
                              </div>
                              <div class="container-fluid" id="product">
                                <div class="row">

                                  <div class="col-sm-12 col-xs-12">
                                    <div class="row">
                                      <div class="col-sm-12">
                                        <h3><?=$sltd_prod_res['productName'];?></h3>
                                      </div>
                                    </div>
                                    <div class="row">
                                      <div class="col-sm-12">
                                        <h5><?=$sltd_prod_res['brand'];?>,&nbsp;<?=$sltd_prod_res['retailer'];?></h5>
                                      </div>
                                    </div>
                                    <div class="row">
                                      <div class="col-sm-8">
                                        <h6>N$&nbsp;<?=$price[0];?><sup><?=$price[1];?></sup></h6>
                                      </div>
                                      <div class="col-sm-4">
                                        <span class="glyphicon glyphicon-star checked"></span>
                                        <span class="glyphicon glyphicon-star checked"></span>
                                        <span class="glyphicon glyphicon-star"></span>
                                        <span class="glyphicon glyphicon-star"></span>
                                        <span class="glyphicon glyphicon-star"></span>
                                      </div>
                                    </div>
                                  </div>
                                </div><hr>
                                <div class="row">
                                  <div class="col-xs-12 col-sm-12">
                                    <div class="container-fluid">
                                      <div class="row">
                                        <div class="col-xs-2 col-sm-2" style="padding:0px;">
                                          <img src="../images/bananasCol1.jpg" alt="" class="img-fluid" style="min-height:40px;">
                                        </div>
                                        <div class="col-xs-2 col-sm-2" style="padding:0px;">
                                          <img src="../images/bananasCol2.jpg" alt="" class="img-fluid" style="min-height:40px;">
                                        </div>
                                        <div class="col-xs-2 col-sm-2" style="padding:0px;">
                                          <img src="../images/bananasCol3.jpg" alt="" class="img-fluid" style="min-height:40px;">
                                        </div>
                                        <div class="col-xs-2 col-sm-2" style="padding:0px;">
                                          <img src="../images/bananasCol4.jpg" alt="" class="img-fluid" style="min-height:40px;">
                                        </div>
                                        <div class="col-xs-2 col-sm-2" style="padding:0px;">
                                          <img src="../images/bananasCol5.jpg" alt="" class="img-fluid" style="min-height:40px;">
                                        </div>
                                      </div>
                                    </div>
                                  </div>
                                </div><br>
                                <div class="row">
                                  <form class="form form-inline" action="index.html" method="post">
                                    <div class="col-sm-6 input-group">
                                      <div class="col-xs-6" style="padding:5px 0px;">
                                        <span class="form-control-feedback">Size</span>
                                        <input type="number" name="" min="1" value="Size" class="col-sm-12 form-control">
                                      </div>
                                      <div class="col-xs-6" style="padding:5px 0px;">
                                      <span class="form-control-feedback">Qty</span>
                                      <input type="number" name="" min="1" value="Quantity" class="col-sm-12 form-control">
                                      </div>
                                    </div>
                                    <div class="col-sm-6">
                                      <input type="button" name="" value="Add to Cart" class="btn btn-primary col-sm-12">
                                    </div>
                                  </form>
                                </div>
                              </div>
                              <div class="container-fluid hidden" id="description">
                                <div class="row">
                                  Description
                                </div>
                              </div>
                              <div class="container-fluid hidden" id="reviews">
                                <div class="row">
                                  reviews
                                </div>
                              </div>
                            </div>
                          </div>
                        </div>
                      </div>
                    </div>
                  </div>
                  <div class="container suggestion-box" style="margin-top: 60px;margin-bottom: 60px;padding-bottom: 10px; border:solid 2px silver; box-shadow: 2px 0px 4px 0px silver; border-radius: 5px 5px;">
                    <h3>You May Also Like</h3>
                    <div class="row">
                      <div class="col-sm-12 jiffy">
                        <div class="container" style="min-height:100px; max-height:150px;">
                          <div class="row">
                            <div class="col-xs-3 col-sm-3 col-md-3 " style="padding:2px;">
                              <img src="../images/apple.jpg" alt="" class="img-fluid">
                            </div>
                            <div class="col-xs-3 col-sm-3 col-md-3 " style="padding:2px;">
                              <img src="../images/orange.jpg" alt="" class="img-fluid">
                            </div>
                            <div class="col-xs-3 col-sm-3 col-md-3 " style="padding:2px;">
                              <img src="../images/avocado.jpg" alt="" class="img-fluid">
                            </div>
                            <div class="col-xs-3 col-sm-3 col-md-3 " style="padding:2px;">
                              <img src="../images/pear.jpg" alt="" class="img-fluid">
                            </div>
                            <div class="hidden-xs col-sm-3 col-md-3 " style="padding:2px;">
                              <img src="../images/grapes.jpg" alt="" class="img-fluid">
                            </div>
                            <div class="hidden-xs col-sm-3 col-md-3 " style="padding:2px;">
                              <img src="../images/pineaple.jpg" alt="" class="img-fluid">
                            </div>
                          </div>
                        </div>
                      </div>
                    </div>
                  </div>
                </main>
                <aside class="bg-light actives" id="sidebar">
                  <?php include_once("../includes/sidebar.php") ?>
                </aside>
                </div>
            </div>
        </div>
        <div class="row">
          <div class="col-xs-12" style="padding:0px;">
            <?php include_once("../includes/footer.php") ?>
          </div>
        </div>
    </div>

    <?php include_once("../includes/scriptTags.php") ?>
    <script type="text/javascript">
    function salesWindow(id){

        var product = document.getElementById("product");
        var decription = document.getElementById("description");
        var reviews = document.getElementById("reviews");

        switch(id){
          case "product":
            product.classList.replace("hidden","visible");
            decription.classList.add('hidden');
            reviews.classList.add('hidden');
            break;
          case "description":
            product.classList.add('hidden');
            decription.classList.replace("hidden","visible");
            reviews.classList.add('hidden');
            break;
          case "reviews":
          product.classList.add('hidden');
          decription.classList.add('hidden');
          reviews.classList.replace("hidden","visible");
          break;
        }
      }


      $("#avopsu").click(function avopsu(){
        var item = ("#avop1");

        alert("kfd");
        item.css("top","100");

      });
    </script>
  </body>
</html>
