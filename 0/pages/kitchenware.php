<!DOCTYPE html>
<html>

<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <link rel="stylesheet" href="../../bootstrap/dist/css/bootstrap.min.css">
    <link rel="stylesheet" href="../../bootstraps/css/bootstrap.min.css">
    <link rel="stylesheet" href="../../stylesheet.css">
    <title>Kitchenware</title>


</head>

<body>
<div class="container-fluid">
    <div class="row">
        <div class="col-xs-12">
            <header class="top">
                <div class="navbar navbar-light bg-light navbar-fixed-top">

                    <div class="col-md-12 navbar-expand-md">
                        <div class="navbar-header">
                            <span class="navbar-brand">acquire</span>
                            <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#kitchenwareMainNav">
                                <span class="navbar-toggler-icon"></span>
                            </button>
                        </div>
                        <div class="collapse navbar-collapse" id="kitchenwareMainNav">
                            <ul class="nav navbar-nav">
                                <li class="nav-item active hidden-xs"><a href="../home.php" class="nav-link"><span class="glyphicon glyphicon-home"></span></a></li>
                                <div class="row nav-justified visible-xs nav-tabs">
                                    <li class="nav-item"><a href="../home.php" class="nav-link col-xs-4 center-block"><span class="glyphicon glyphicon-home"></span></a></li>
                                    <li class="nav-item"><a href="#" class="nav-link col-xs-4 center-block"><span class="glyphicon glyphicon-shopping-cart"></span></a> </li>
                                    <li class="nav-item"><a href="#loginModalMenu" class="nav-link col-xs-4 center-block" data-toggle="modal"><span class="glyphicon glyphicon-user"></span></a></li>
                                </div>
                            </ul>
                            <div class="col-sm-12 col-sm-offset-10 visible-xs">
                                <div class="dropdown">
                                    <a href="#" class="dropdown-toggle btn btn-outline-dark" data-toggle="dropdown">Retailers</a>
                                    <div class="dropdown-menu">
                                        <a class="dropdown-item" href="#">Shop1</a>
                                        <a class="dropdown-item" href="#">Shop2</a>
                                        <a class="dropdown-item" href="#">Shop3</a>
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-xs-11 col-md-8">
                                    <form class="navbar-form form-horizontal" role="search">
                                        <div class="row">
                                            <div class="form-group col-sm-12">
                                                <div class="input-group">
                                                    <input type="text" class="form-control" placeholder="search" >
                                                    <span class="input-group-append">
                                                    <button type="submit" class="btn btn-default">
                                                        <span class="glyphicon glyphicon-search"></span>
                                                    </button>
                                                </span>
                                                </div>
                                            </div>
                                        </div>
                                    </form>
                                </div>
                                <div class="col-xs-4">
                                    <div class="pull-right">
                                        <ul class="nav navbar-nav">
                                            <li class="nav-item hidden-xs"><a href="#" class="nav-link"><span class="glyphicon glyphicon-shopping-cart"></span></a> </li>
                                            <li class="nav-item hidden-xs"><a href="#loginModalMenu" class="nav-link" data-toggle="modal"><span class="glyphicon glyphicon-user"></span></a></li>
                                        </ul>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-xs-12">
                        <div class="dropdown">
                            <a href="#" class="dropdown-toggle col-xs-12 bg-danger" data-toggle="dropdown"></a>
                            <div class="dropdown-menu col-xs-12">
                                <div class="dropdown-item">
                                    <ul class="breadcrumb">
                                        <li class="breadcrumb-item"><a href="../clothing/clothing.php">Clothing</a></li>
                                        <li class="breadcrumb-item">
                                            <a href="../groceries/groceries.php">Food &amp; Grocery</a>
                                        </li>
                                        <li class="breadcrumb-item">
                                            <a href="../accessories/accessories.php">Accessories</a>
                                        </li>
                                        <li class="breadcrumb-item">
                                            <a href="../footwear/footwear.php">Footwear</a>
                                        </li>
                                        <li class="breadcrumb-item">
                                            <a href="../cosmetics/cosmetics.php">Cosmetics</a>
                                        </li>
                                        <li class="breadcrumb-item">
                                            <a href="../gadgets/gadgets.php">Gadgets &amp; Electronics</a>
                                        </li>
                                    </ul>
                                </div>
                                <div class="dropdown-item">
                                    <ul class="breadcrumb">
                                        <li class="breadcrumb-item"><a href="clothing/clothing.php">Men</a></li>
                                        <li class="breadcrumb-item">
                                            <a href="groceries/groceries.php">Ladies</a>
                                        </li>
                                        <li class="breadcrumb-item">
                                            <a href="accessories/accessories.php">Kids</a>
                                        </li>
                                        <li class="breadcrumb-item">
                                            <a href="footwear/footwear.php">Toddlers</a>
                                        </li>
                                        <li class="breadcrumb-item">
                                            <a href="footwear/footwear.php">Infants</a>
                                        </li>
                                    </ul>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="modal" id="loginModalMenu" tabindex="-1">
                    <div class="modal-dialog">
                        <div class="modal-content">
                            <div class="modal-header">
                                <h4 class="modal-title">Login</h4>
                                <button class="btn close" data-dismiss="modal">&times;</button>
                            </div>
                            <div class="modal-body">
                                <form>
                                    <div class="form-group">
                                        <div class="input-group">
                                            <label for="username" class="control-label">Username</label>
                                            <input type="text" id="username" placeholder="Enter username" class="form-control">
                                        </div>
                                        <div class="input-group">
                                            <label for="password" class="control-label">Password</label>
                                            <input type="password" id="password" placeholder="Enter password" class="form-control">
                                        </div>
                                    </div>
                                </form>
                            </div>
                            <div class="modal-footer">
                                <button type="button" class="btn">Login</button>
                            </div>
                        </div>
                    </div>
                </div>
            </header>
            <!-- end of top part -->
            <main>
                <div class="container">
                    <h4>Daily Bargains</h4>
                    <div id="foodCarousel" class="carousel slide" data-ride="carousel" data-interval="false" data-wrap="false">
                        <div class="carousel-inner">
                            <div class="carousel-item active">
                                <div class="container">
                                    <div class="row">
                                        <div class="col-xs-6 col-sm-3 col-lg-2">
                                            <a href="#">
                                                <img src="shrimp.png" class="img-fluid" />
                                            </a>
                                            <p class="text-justify text-left">Item name:
                                                <br/> Price:
                                                <br /> Sizes:
                                                <br /> Colors:
                                                <br /> Availability:
                                                <br /> Retailer:
                                            </p>
                                        </div>
                                        <div class="col-xs-6 col-sm-3 col-lg-2">
                                            <a href="#">
                                                <img src="pizza.png" class="img-fluid" />
                                            </a>
                                            <p class="text-justify text-left">Item name:
                                                <br/> Price:
                                                <br /> Sizes:
                                                <br /> Colors:
                                                <br /> Availability:
                                                <br /> Retailer:
                                            </p>
                                        </div>
                                        <div class="col-xs-6 col-sm-3 col-lg-2">
                                            <a href="#">
                                                <img src="oranges.png" class="img-fluid" />
                                            </a>
                                            <p class="text-justify text-left">Item name:
                                                <br/> Price:
                                                <br /> Sizes:
                                                <br /> Colors:
                                                <br /> Availability:
                                                <br /> Retailer:
                                            </p>
                                        </div>
                                        <div class="col-xs-6 col-sm-3 col-lg-2">
                                            <a href="#">
                                                <img src="ham.png" class="img-fluid" />
                                            </a>
                                            <p class="text-justify text-left">Item name:
                                                <br/> Price:
                                                <br /> Sizes:
                                                <br /> Colors:
                                                <br /> Availability:
                                                <br /> Retailer:
                                            </p>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="carousel-item">
                                <div class="container">
                                    <div class="row">
                                        <div class="col-xs-6 col-sm-3 col-lg-2">
                                            <a href="#">
                                                <img src="bananas.png" class="img-fluid" />
                                            </a>
                                            <p>Item name:
                                                <br/> Price:
                                                <br /> Sizes:
                                                <br /> Colors:
                                                <br /> Availability:
                                                <br /> Retailer:
                                            </p>
                                        </div>
                                        <div class="col-xs-6 col-sm-3 col-lg-2">
                                            <a href="#">
                                                <img src="tomatoes.jpg" class="img-fluid" />
                                            </a>
                                            <p>Item name:
                                                <br/> Price:
                                                <br /> Sizes:
                                                <br /> Colors:
                                                <br /> Availability:
                                                <br /> Retailer:
                                            </p>
                                        </div>
                                        <div class="col-xs-6 col-sm-3 col-lg-2">
                                            <a href="#">
                                                <img src="snacks-assorted.jpg" class="img-fluid" />
                                            </a>
                                            <p>Item name:
                                                <br/> Price:
                                                <br /> Sizes:
                                                <br /> Colors:
                                                <br /> Availability:
                                                <br /> Retailer:
                                            </p>
                                        </div>
                                        <div class="col-xs-6 col-sm-3 col-lg-2">
                                            <a href="#">
                                                <img src="mateys.png" class="img-fluid" />
                                            </a>
                                            <p>Item name:
                                                <br/> Price:
                                                <br /> Sizes:
                                                <br /> Colors:
                                                <br /> Availability:
                                                <br /> Retailer:
                                            </p>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div>
                            <a href="#foodCarousel" class="carousel-control-prev" data-slide="prev">
                                <span class="carousel-control-prev-icon"></span>
                            </a>
                            <a href="#foodCarousel" class="carousel-control-next" data-slide="next">
                                <span class="carousel-control-next-icon"></span>
                            </a>
                        </div>
                    </div>
                </div>
                <div class="container">
                    <h4>Utensils</h4>
                    <div id="utensilsCarousel" class="carousel slide" data-ride="carousel" data-interval="false" data-wrap="false">
                        <div class="carousel-inner">
                            <div class="carousel-item active">
                                <div class="container">
                                    <div class="row">
                                        <div class="col-xs-6 col-sm-3 col-lg-2">
                                            <a href="#">
                                                <img src="shrimp.png" class="img-fluid" />
                                            </a>
                                            <p class="text-justify text-left">Item name:
                                                <br/> Price:
                                                <br /> Sizes:
                                                <br /> Colors:
                                                <br /> Availability:
                                                <br /> Retailer:
                                            </p>
                                        </div>
                                        <div class="col-xs-6 col-sm-3 col-lg-2">
                                            <a href="#">
                                                <img src="pizza.png" class="img-fluid" />
                                            </a>
                                            <p class="text-justify text-left">Item name:
                                                <br/> Price:
                                                <br /> Sizes:
                                                <br /> Colors:
                                                <br /> Availability:
                                                <br /> Retailer:
                                            </p>
                                        </div>
                                        <div class="col-xs-6 col-sm-3 col-lg-2">
                                            <a href="#">
                                                <img src="oranges.png" class="img-fluid" />
                                            </a>
                                            <p class="text-justify text-left">Item name:
                                                <br/> Price:
                                                <br /> Sizes:
                                                <br /> Colors:
                                                <br /> Availability:
                                                <br /> Retailer:
                                            </p>
                                        </div>
                                        <div class="col-xs-6 col-sm-3 col-lg-2">
                                            <a href="#">
                                                <img src="ham.png" class="img-fluid" />
                                            </a>
                                            <p class="text-justify text-left">Item name:
                                                <br/> Price:
                                                <br /> Sizes:
                                                <br /> Colors:
                                                <br /> Availability:
                                                <br /> Retailer:
                                            </p>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="carousel-item">
                                <div class="container">
                                    <div class="row">
                                        <div class="col-xs-6 col-sm-3 col-lg-2">
                                            <a href="#">
                                                <img src="bananas.png" class="img-fluid" />
                                            </a>
                                            <p>Item name:
                                                <br/> Price:
                                                <br /> Sizes:
                                                <br /> Colors:
                                                <br /> Availability:
                                                <br /> Retailer:
                                            </p>
                                        </div>
                                        <div class="col-xs-6 col-sm-3 col-lg-2">
                                            <a href="#">
                                                <img src="tomatoes.jpg" class="img-fluid" />
                                            </a>
                                            <p>Item name:
                                                <br/> Price:
                                                <br /> Sizes:
                                                <br /> Colors:
                                                <br /> Availability:
                                                <br /> Retailer:
                                            </p>
                                        </div>
                                        <div class="col-xs-6 col-sm-3 col-lg-2">
                                            <a href="#">
                                                <img src="snacks-assorted.jpg" class="img-fluid" />
                                            </a>
                                            <p>Item name:
                                                <br/> Price:
                                                <br /> Sizes:
                                                <br /> Colors:
                                                <br /> Availability:
                                                <br /> Retailer:
                                            </p>
                                        </div>
                                        <div class="col-xs-6 col-sm-3 col-lg-2">
                                            <a href="#">
                                                <img src="mateys.png" class="img-fluid" />
                                            </a>
                                            <p>Item name:
                                                <br/> Price:
                                                <br /> Sizes:
                                                <br /> Colors:
                                                <br /> Availability:
                                                <br /> Retailer:
                                            </p>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div>
                            <a href="#utensilsCarousel" class="carousel-control-prev" data-slide="prev">
                                <span class="carousel-control-prev-icon"></span>
                            </a>
                            <a href="#utensilsCarousel" class="carousel-control-next" data-slide="next">
                                <span class="carousel-control-next-icon"></span>
                            </a>
                        </div>
                    </div>
                </div>
                <div class="container">
                    <h4>Cups &amp; Plates</h4>
                    <div id="cupsAndPlatesCarousel" class="carousel slide" data-ride="carousel" data-interval="false" data-wrap="false">
                        <div class="carousel-inner">
                            <div class="carousel-item active">
                                <div class="container">
                                    <div class="row">
                                        <div class="col-xs-6 col-sm-3 col-lg-2">
                                            <a href="#">
                                                <img src="shrimp.png" class="img-fluid" />
                                            </a>
                                            <p class="text-justify text-left">Item name:
                                                <br/> Price:
                                                <br /> Sizes:
                                                <br /> Colors:
                                                <br /> Availability:
                                                <br /> Retailer:
                                            </p>
                                        </div>
                                        <div class="col-xs-6 col-sm-3 col-lg-2">
                                            <a href="#">
                                                <img src="pizza.png" class="img-fluid" />
                                            </a>
                                            <p class="text-justify text-left">Item name:
                                                <br/> Price:
                                                <br /> Sizes:
                                                <br /> Colors:
                                                <br /> Availability:
                                                <br /> Retailer:
                                            </p>
                                        </div>
                                        <div class="col-xs-6 col-sm-3 col-lg-2">
                                            <a href="#">
                                                <img src="oranges.png" class="img-fluid" />
                                            </a>
                                            <p class="text-justify text-left">Item name:
                                                <br/> Price:
                                                <br /> Sizes:
                                                <br /> Colors:
                                                <br /> Availability:
                                                <br /> Retailer:
                                            </p>
                                        </div>
                                        <div class="col-xs-6 col-sm-3 col-lg-2">
                                            <a href="#">
                                                <img src="ham.png" class="img-fluid" />
                                            </a>
                                            <p class="text-justify text-left">Item name:
                                                <br/> Price:
                                                <br /> Sizes:
                                                <br /> Colors:
                                                <br /> Availability:
                                                <br /> Retailer:
                                            </p>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="carousel-item">
                                <div class="container">
                                    <div class="row">
                                        <div class="col-xs-6 col-sm-3 col-lg-2">
                                            <a href="#">
                                                <img src="bananas.png" class="img-fluid" />
                                            </a>
                                            <p>Item name:
                                                <br/> Price:
                                                <br /> Sizes:
                                                <br /> Colors:
                                                <br /> Availability:
                                                <br /> Retailer:
                                            </p>
                                        </div>
                                        <div class="col-xs-6 col-sm-3 col-lg-2">
                                            <a href="#">
                                                <img src="tomatoes.jpg" class="img-fluid" />
                                            </a>
                                            <p>Item name:
                                                <br/> Price:
                                                <br /> Sizes:
                                                <br /> Colors:
                                                <br /> Availability:
                                                <br /> Retailer:
                                            </p>
                                        </div>
                                        <div class="col-xs-6 col-sm-3 col-lg-2">
                                            <a href="#">
                                                <img src="snacks-assorted.jpg" class="img-fluid" />
                                            </a>
                                            <p>Item name:
                                                <br/> Price:
                                                <br /> Sizes:
                                                <br /> Colors:
                                                <br /> Availability:
                                                <br /> Retailer:
                                            </p>
                                        </div>
                                        <div class="col-xs-6 col-sm-3 col-lg-2">
                                            <a href="#">
                                                <img src="mateys.png" class="img-fluid" />
                                            </a>
                                            <p>Item name:
                                                <br/> Price:
                                                <br /> Sizes:
                                                <br /> Colors:
                                                <br /> Availability:
                                                <br /> Retailer:
                                            </p>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div>
                            <a href="#cupsAndPlatesCarousel" class="carousel-control-prev" data-slide="prev">
                                <span class="carousel-control-prev-icon"></span>
                            </a>
                            <a href="#cupsAndPlatesCarousel" class="carousel-control-next" data-slide="next">
                                <span class="carousel-control-next-icon"></span>
                            </a>
                        </div>
                    </div>
                </div>
                <div class="container">
                    <h4>Pots &amp Pans; </h4>
                    <div id="potsAndPansCarousel" class="carousel slide" data-ride="carousel" data-interval="false" data-wrap="false">
                        <div class="carousel-inner">
                            <div class="carousel-item active">
                                <div class="container">
                                    <div class="row">
                                        <div class="col-xs-6 col-sm-3 col-lg-2">
                                            <a href="#">
                                                <img src="shrimp.png" class="img-fluid" />
                                            </a>
                                            <p class="text-justify text-left">Item name:
                                                <br/> Price:
                                                <br /> Sizes:
                                                <br /> Colors:
                                                <br /> Availability:
                                                <br /> Retailer:
                                            </p>
                                        </div>
                                        <div class="col-xs-6 col-sm-3 col-lg-2">
                                            <a href="#">
                                                <img src="pizza.png" class="img-fluid" />
                                            </a>
                                            <p class="text-justify text-left">Item name:
                                                <br/> Price:
                                                <br /> Sizes:
                                                <br /> Colors:
                                                <br /> Availability:
                                                <br /> Retailer:
                                            </p>
                                        </div>
                                        <div class="col-xs-6 col-sm-3 col-lg-2">
                                            <a href="#">
                                                <img src="oranges.png" class="img-fluid" />
                                            </a>
                                            <p class="text-justify text-left">Item name:
                                                <br/> Price:
                                                <br /> Sizes:
                                                <br /> Colors:
                                                <br /> Availability:
                                                <br /> Retailer:
                                            </p>
                                        </div>
                                        <div class="col-xs-6 col-sm-3 col-lg-2">
                                            <a href="#">
                                                <img src="ham.png" class="img-fluid" />
                                            </a>
                                            <p class="text-justify text-left">Item name:
                                                <br/> Price:
                                                <br /> Sizes:
                                                <br /> Colors:
                                                <br /> Availability:
                                                <br /> Retailer:
                                            </p>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="carousel-item">
                                <div class="container">
                                    <div class="row">
                                        <div class="col-xs-6 col-sm-3 col-lg-2">
                                            <a href="#">
                                                <img src="bananas.png" class="img-fluid" />
                                            </a>
                                            <p>Item name:
                                                <br/> Price:
                                                <br /> Sizes:
                                                <br /> Colors:
                                                <br /> Availability:
                                                <br /> Retailer:
                                            </p>
                                        </div>
                                        <div class="col-xs-6 col-sm-3 col-lg-2">
                                            <a href="#">
                                                <img src="tomatoes.jpg" class="img-fluid" />
                                            </a>
                                            <p>Item name:
                                                <br/> Price:
                                                <br /> Sizes:
                                                <br /> Colors:
                                                <br /> Availability:
                                                <br /> Retailer:
                                            </p>
                                        </div>
                                        <div class="col-xs-6 col-sm-3 col-lg-2">
                                            <a href="#">
                                                <img src="snacks-assorted.jpg" class="img-fluid" />
                                            </a>
                                            <p>Item name:
                                                <br/> Price:
                                                <br /> Sizes:
                                                <br /> Colors:
                                                <br /> Availability:
                                                <br /> Retailer:
                                            </p>
                                        </div>
                                        <div class="col-xs-6 col-sm-3 col-lg-2">
                                            <a href="#">
                                                <img src="mateys.png" class="img-fluid" />
                                            </a>
                                            <p>Item name:
                                                <br/> Price:
                                                <br /> Sizes:
                                                <br /> Colors:
                                                <br /> Availability:
                                                <br /> Retailer:
                                            </p>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div>
                            <a href="#potsAndPansCarousel" class="carousel-control-prev" data-slide="prev">
                                <span class="carousel-control-prev-icon"></span>
                            </a>
                            <a href="#potsAndPansCarousel" class="carousel-control-next" data-slide="next">
                                <span class="carousel-control-next-icon"></span>
                            </a>
                        </div>
                    </div>
                </div>
                <div class="container">
                    <h4>Cleaning Products</h4>
                    <div id="cleaningProductsCarousel" class="carousel slide" data-ride="carousel" data-interval="false" data-wrap="false">
                        <div class="carousel-inner">
                            <div class="carousel-item active">
                                <div class="container">
                                    <div class="row">
                                        <div class="col-xs-6 col-sm-3 col-lg-2">
                                            <a href="#">
                                                <img src="shrimp.png" class="img-fluid" />
                                            </a>
                                            <p class="text-justify text-left">Item name:
                                                <br/> Price:
                                                <br /> Sizes:
                                                <br /> Colors:
                                                <br /> Availability:
                                                <br /> Retailer:
                                            </p>
                                        </div>
                                        <div class="col-xs-6 col-sm-3 col-lg-2">
                                            <a href="#">
                                                <img src="pizza.png" class="img-fluid" />
                                            </a>
                                            <p class="text-justify text-left">Item name:
                                                <br/> Price:
                                                <br /> Sizes:
                                                <br /> Colors:
                                                <br /> Availability:
                                                <br /> Retailer:
                                            </p>
                                        </div>
                                        <div class="col-xs-6 col-sm-3 col-lg-2">
                                            <a href="#">
                                                <img src="oranges.png" class="img-fluid" />
                                            </a>
                                            <p class="text-justify text-left">Item name:
                                                <br/> Price:
                                                <br /> Sizes:
                                                <br /> Colors:
                                                <br /> Availability:
                                                <br /> Retailer:
                                            </p>
                                        </div>
                                        <div class="col-xs-6 col-sm-3 col-lg-2">
                                            <a href="#">
                                                <img src="ham.png" class="img-fluid" />
                                            </a>
                                            <p class="text-justify text-left">Item name:
                                                <br/> Price:
                                                <br /> Sizes:
                                                <br /> Colors:
                                                <br /> Availability:
                                                <br /> Retailer:
                                            </p>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="carousel-item">
                                <div class="container">
                                    <div class="row">
                                        <div class="col-xs-6 col-sm-3 col-lg-2">
                                            <a href="#">
                                                <img src="bananas.png" class="img-fluid" />
                                            </a>
                                            <p>Item name:
                                                <br/> Price:
                                                <br /> Sizes:
                                                <br /> Colors:
                                                <br /> Availability:
                                                <br /> Retailer:
                                            </p>
                                        </div>
                                        <div class="col-xs-6 col-sm-3 col-lg-2">
                                            <a href="#">
                                                <img src="tomatoes.jpg" class="img-fluid" />
                                            </a>
                                            <p>Item name:
                                                <br/> Price:
                                                <br /> Sizes:
                                                <br /> Colors:
                                                <br /> Availability:
                                                <br /> Retailer:
                                            </p>
                                        </div>
                                        <div class="col-xs-6 col-sm-3 col-lg-2">
                                            <a href="#">
                                                <img src="snacks-assorted.jpg" class="img-fluid" />
                                            </a>
                                            <p>Item name:
                                                <br/> Price:
                                                <br /> Sizes:
                                                <br /> Colors:
                                                <br /> Availability:
                                                <br /> Retailer:
                                            </p>
                                        </div>
                                        <div class="col-xs-6 col-sm-3 col-lg-2">
                                            <a href="#">
                                                <img src="mateys.png" class="img-fluid" />
                                            </a>
                                            <p>Item name:
                                                <br/> Price:
                                                <br /> Sizes:
                                                <br /> Colors:
                                                <br /> Availability:
                                                <br /> Retailer:
                                            </p>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div>
                            <a href="#cleaningProductsCarousel" class="carousel-control-prev" data-slide="prev">
                                <span class="carousel-control-prev-icon"></span>
                            </a>
                            <a href="#cleaningProductsCarousel" class="carousel-control-next" data-slide="next">
                                <span class="carousel-control-next-icon"></span>
                            </a>
                        </div>

                    </div>
                </div>
                <div class="container">
                    <h4>Equipment</h4>
                    <div id="equipmentCarousel" class="carousel slide" data-ride="carousel" data-interval="false" data-wrap="false">
                        <div class="carousel-inner">
                            <div class="carousel-item active">
                                <div class="container">
                                    <div class="row">
                                        <div class="col-xs-6 col-sm-3 col-lg-2">
                                            <a href="#">
                                                <img src="shrimp.png" class="img-fluid" />
                                            </a>
                                            <p class="text-justify text-left">Item name:
                                                <br/> Price:
                                                <br /> Sizes:
                                                <br /> Colors:
                                                <br /> Availability:
                                                <br /> Retailer:
                                            </p>
                                        </div>
                                        <div class="col-xs-6 col-sm-3 col-lg-2">
                                            <a href="#">
                                                <img src="pizza.png" class="img-fluid" />
                                            </a>
                                            <p class="text-justify text-left">Item name:
                                                <br/> Price:
                                                <br /> Sizes:
                                                <br /> Colors:
                                                <br /> Availability:
                                                <br /> Retailer:
                                            </p>
                                        </div>
                                        <div class="col-xs-6 col-sm-3 col-lg-2">
                                            <a href="#">
                                                <img src="oranges.png" class="img-fluid" />
                                            </a>
                                            <p class="text-justify text-left">Item name:
                                                <br/> Price:
                                                <br /> Sizes:
                                                <br /> Colors:
                                                <br /> Availability:
                                                <br /> Retailer:
                                            </p>
                                        </div>
                                        <div class="col-xs-6 col-sm-3 col-lg-2">
                                            <a href="#">
                                                <img src="ham.png" class="img-fluid" />
                                            </a>
                                            <p class="text-justify text-left">Item name:
                                                <br/> Price:
                                                <br /> Sizes:
                                                <br /> Colors:
                                                <br /> Availability:
                                                <br /> Retailer:
                                            </p>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="carousel-item">
                                <div class="container">
                                    <div class="row">
                                        <div class="col-xs-6 col-sm-3 col-lg-2">
                                            <a href="#">
                                                <img src="bananas.png" class="img-fluid" />
                                            </a>
                                            <p>Item name:
                                                <br/> Price:
                                                <br /> Sizes:
                                                <br /> Colors:
                                                <br /> Availability:
                                                <br /> Retailer:
                                            </p>
                                        </div>
                                        <div class="col-xs-6 col-sm-3 col-lg-2">
                                            <a href="#">
                                                <img src="tomatoes.jpg" class="img-fluid" />
                                            </a>
                                            <p>Item name:
                                                <br/> Price:
                                                <br /> Sizes:
                                                <br /> Colors:
                                                <br /> Availability:
                                                <br /> Retailer:
                                            </p>
                                        </div>
                                        <div class="col-xs-6 col-sm-3 col-lg-2">
                                            <a href="#">
                                                <img src="snacks-assorted.jpg" class="img-fluid" />
                                            </a>
                                            <p>Item name:
                                                <br/> Price:
                                                <br /> Sizes:
                                                <br /> Colors:
                                                <br /> Availability:
                                                <br /> Retailer:
                                            </p>
                                        </div>
                                        <div class="col-xs-6 col-sm-3 col-lg-2">
                                            <a href="#">
                                                <img src="mateys.png" class="img-fluid" />
                                            </a>
                                            <p>Item name:
                                                <br/> Price:
                                                <br /> Sizes:
                                                <br /> Colors:
                                                <br /> Availability:
                                                <br /> Retailer:
                                            </p>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div>
                            <a href="#equipmentCarousel" class="carousel-control-prev" data-slide="prev">
                                <span class="carousel-control-prev-icon"></span>
                            </a>
                            <a href="#equipmentCarousel" class="carousel-control-next" data-slide="next">
                                <span class="carousel-control-next-icon"></span>
                            </a>
                        </div>

                    </div>
                </div>
            </main>
            <footer>
                <div class="container-fluid">
                    <ul>
                        <li>About</li>
                        <li>Contact</li>
                        <li>Social media</li>
                        <li>Help</li>
                    </ul>
                    <span>&copy;Copyright</span>
                </div>
            </footer>
        </div>
    </div>
</div>
<script src="../../bootstrap/assets/js/vendor/jquery-slim.min.js"></script>
<script src="../../bootstrap/assets/js/vendor/popper.min.js"></script>
<script src="../../bootstrap/dist/js/bootstrap.min.js"></script>
</body>

</html>
