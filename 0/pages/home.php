<html>
<head>
  <?php include_once("../includes/headTags.php") ?>
  <link rel="stylesheet" href="../css_files/global_css.css">
  <title><?php $pageTitle='home'; echo $pageTitle; ?></title>
</head>
<body>
          <?php include("../includes/header.php") ?>
            <!-- end of top part -->
          <main class="">
            <div id="top-banner" class="carousel slide" data-ride="carousel" data-wrap="true" data-interval="4000" style="min-height: 120px;width: 100%;">
              <ul class="carousel-indicators">
                <li data-target="#top-banner" class="active" data-slide-to="0"></li>
                <li data-target="#top-banner" data-slide-to="1"></li>
              </ul>
              <div class="carousel-inner" style="list-style:none; padding:0px;" >
                <div class="carousel-item active" style="padding:0px; height: 200px;">
                  <div style="width: 49%; height: 100%; background-color: orange; display:inline-block;"></div>
                  <div style="width: 49%; height: 100%; background-color:orange; display:inline-block;"></div>
                </div>
                <div class="carousel-item" style="padding:0px; height: 200px;">
                  <div style="width: 100%; height: 100%; background-color: gold; display:inline-block;"></div>
                </div>
                <a href="#top-banner" class="carousel-control-prev" data-slide="prev">
                  <span class="glyphicon glyphicon-chevron-left"></span>
                </a>
                <a href="#top-banner" class="carousel-control-next" data-slide="next">
                  <span class="glyphicon glyphicon-chevron-right"></span>
                </a>
              </div>
            </div>
			<br><br><br>
            <div class="" style="height:170px; position: relative;white-space: nowrap; overflow:hidden;">
              <div class="hexagon"></div>
              <div class="hexagon"></div>
              <div class="hexagon"></div>
              <div class="hexagon"></div>
            </div>
			<?php include("../includes/productsContainer.php");?>
			
            <div class="normal-single-display" style="height:250px; background-color:orange;">
              <div class="col-xs-12 col-sm-6 col-md-6" style="height: 100%; padding:0px; background-color:blue; display:inline-block;">
                <div class="col-xs-8" style="display:inline-block; height: inherit;">

                </div>
                <div class="col-xs-4" style="display:inline-block; height: inherit;background-color: yellow;">
                  <div class="">Type of advert <span>store</span></div><br><br>
                  <div class="">Description/Name</div><br><br>
                  <div class=""><span>Was</span><span>Now</span></div><br><br>
                  <div class=""><button type="button" class="btn add-to-cart-btn" name="button">Add to cart</button> </div>
                </div>
              </div>
              <div class="visible-sm visible-md visible-lg col-sm-6 col-md-6" style="height: 100%; background-color:red; display:inline-block;">
                <div class="col-xs-8" style="display:inline-block; height: inherit;">

                </div>
                <div class="col-xs-4" style="display:inline-block; height: inherit;background-color: yellow;">
                  <div class="">Type of advert <span>store</span></div><br><br>
                  <div class="">Description/Name</div><br><br>
                  <div class=""><span>Was</span><span>Now</span></div><br><br>
                  <div class=""><button type="button" class="btn add-to-cart-btn" name="button">Add to cart</button> </div>
                </div>
              </div>
            </div>
            <br><br><br>
            <div class="static-banner" style="height:239px; background-color:orange;">
            </div>
          </main>
          <aside class="bg-light actives" id="sidebar">
            <?php include_once("../includes/sidebar.php") ?>
          </aside>

    <div class="row">
      <div class="col-xs-12" style="padding:0px;">
        <?php //include_once("../includes/footer.php") ?>
      </div>
    </div>
<?php include_once("../includes/scriptTags.php") ?>
<script src="../js_files/global_javascript.js"></script>
</body>
</html>
