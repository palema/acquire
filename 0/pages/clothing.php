<!DOCTYPE html>
<?php
session_start();
require '../../connect.php';

?>
<html>
<head>
  <?php include_once("../../headTags.php") ?>
  <style media="screen">
  </style>
  <title><?php $pageTitle='clothing'; echo $pageTitle; ?></title>
</head>
<body>
<div class="container-fluid">
    <div class="row">
        <div class="col-xs-12">
          <?php include("../../header.php") ?>
            <!-- end of top part -->
            <div class="row">
            <main class="col-xs-12 col-md-12">
              <div class="container-fluid">
                <div class="row">
                  <div class="col-xs-12">
                    advert
                  </div>
                </div>
              </div>
              <?php include("../../productsContainer.php") ?>
            </main>
            <aside class="bg-light actives" id="sidebar">
              <?php include_once("../../sidebar.php") ?>
            </aside>
            </div>
        </div>
    </div>
    <div class="row" style="box-shadow:-1px 0px 6px;">
      <div class="col-xs-12" style="padding:0px;">
        <?php include_once("../../footer.php") ?>
      </div>
    </div>
</div>

<?php include_once("../../scriptTags.php") ?>
<!--carousel indicator control visibility-->
<script type="text/javascript">
$(document).ready(function() {
$(".carousel").each(function(){
  s=$("#"+$(this).attr("id")+" .carousel-item").length;
  if(s<2){
    $(this).find('.carousel-control-next').addClass('hidden').removeClass('visible');
  }
});
$(".carousel").on("slide.bs.carousel", function(e) {
  var $e = $(e.relatedTarget);
  var idx = $e.index();
  var itemsPerSlide = 3;
  var totalItems = $("#"+$(this).attr("id")+" .carousel-item").length;
  if(idx!=0){
    $(this).find('.carousel-control-prev').addClass('visible').removeClass('hidden');
  }else{
    $(this).find('.carousel-control-prev').addClass('hidden').removeClass('visible');
  }

  if((idx+1)<totalItems){
    $(this).find('.carousel-control-next').addClass('visible').removeClass('hidden');
  }else{
    $(this).find('.carousel-control-next').addClass('hidden').removeClass('visible');
  }
  // if (idx >= totalItems - (itemsPerSlide - 1)) {
  //   var it = itemsPerSlide - (totalItems - idx);
  //   for (var i = 0; i < it; i++) {
  //     // append slides to end
  //     if (e.direction == "left") {
  //       $(".carousel-item")
  //         .eq(i)
  //         .appendTo(".carousel-inner");
  //     } else {
  //       $(".carousel-item")
  //         .eq(0)
  //         .appendTo($(this).find(".carousel-inner"));
  //     }
  //   }
  // }
});
});
</script>
</body>
</html>
